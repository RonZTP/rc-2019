/*----------------------------------------------------------------------------*/
/* Copyright (c) 2018 FIRST. All Rights Reserved.                             */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot.subsystems;

import edu.wpi.first.wpilibj.VictorSP;
import edu.wpi.first.wpilibj.command.Subsystem;
import frc.robot.RobotMap;
import frc.robot.commands.HandControlClimbing;

/**
 * Add your docs here.
 */
public class Climbing extends Subsystem {
  // Put methods for controlling this subsystem
  // here. Call these from Commands.
  VictorSP motorA;
  VictorSP motorB;
  public double speedOfMotor = -1.0;
  public Climbing(){
    motorA = new VictorSP(RobotMap.ClimbingA);
    //motorB = new VictorSP(RobotMap.ClimbingB);
  }

  public double getClimbingSpeed()
  {
    return this.speedOfMotor;
  }

  public void setRaw(double speed){
    motorA.set(speed);
    //motorB.set(speed);
  }

  @Override
  public void initDefaultCommand() {
    // Set the default command for a subsystem here.
    // setDefaultCommand(new MySpecialCommand());
    setDefaultCommand(new HandControlClimbing());
  }
}
