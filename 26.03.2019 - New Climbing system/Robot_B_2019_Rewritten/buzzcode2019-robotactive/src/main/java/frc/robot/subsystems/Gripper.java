/*----------------------------------------------------------------------------*/
/* Copyright (c) 2018 FIRST. All Rights Reserved.                             */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot.subsystems;

import edu.wpi.first.wpilibj.DigitalInput;
import edu.wpi.first.wpilibj.DoubleSolenoid;
import edu.wpi.first.wpilibj.Encoder;
import edu.wpi.first.wpilibj.VictorSP;
import edu.wpi.first.wpilibj.DoubleSolenoid.Value;
import edu.wpi.first.wpilibj.GenericHID.RumbleType;
import edu.wpi.first.wpilibj.command.Subsystem;
import frc.robot.Robot;
import frc.robot.RobotMap;
import frc.robot.commands.HandControlGripper;

/**
 * Add your docs here.
 */
public class Gripper extends Subsystem {
  // Put methods for controlling this subsystem
  // here. Call these from Commands.
  private VictorSP Up;
  private VictorSP Down;
  private VictorSP Fold;
  private DigitalInput limit;
  private Encoder encoder;
  private double LIMIT_DOWN = 11400;
  private DoubleSolenoid HatchFold;

  public Gripper(){
    Up = new VictorSP(RobotMap.GRIPPER_UP);
    Down = new VictorSP(RobotMap.GRIPPER_DOWN);
    Fold = new VictorSP(RobotMap.GRIPPER_FOLDING);
    limit = new DigitalInput(RobotMap.GRIPPER_LIMIT);
    Fold.setInverted(true);

    HatchFold = new DoubleSolenoid(RobotMap.HATCH_SOLENOID_FORWARD, RobotMap.HATCH_SOLENOID_REVERSE);

    encoder = new Encoder(6,7,false,Encoder.EncodingType.k4X);
    encoder.setMaxPeriod(.1);
    encoder.setMinRate(10);
    encoder.setDistancePerPulse(5);
    encoder.setReverseDirection(true);
    encoder.setSamplesToAverage(7);
    encoder.reset();
  }

  public void setSpeedVersa(double upSpeed,double downSpeed){
    Up.set(upSpeed);
    Down.set(downSpeed);
  }

  public void setSpeedFolding(double speed){
    if((this.getEncoerValue()>LIMIT_DOWN && speed>0.0) || (!this.getLimit() && speed<0.0)){
      speed=0.0;   
      Robot.oi.getXboxController().setRumble(RumbleType.kLeftRumble, 1.0);
      Robot.oi.getXboxController().setRumble(RumbleType.kRightRumble, 1.0);
    }else{
      if((this.getEncoerValue()>LIMIT_DOWN*0.8 && speed>0.0))
      {
        speed*=0.4;
      }
      Robot.oi.getXboxController().setRumble(RumbleType.kLeftRumble, 0.0);
      Robot.oi.getXboxController().setRumble(RumbleType.kRightRumble, 0.0);
    }
    
    System.out.println(this.getEncoerValue());
    this.Fold.set(speed);


   // System.out.println("speed of fold is:" + speed);
      
  }

  public void foldHatchUp(){
    if(Robot.elevator.getLimitDown().get()){
      this.HatchFold.set(Value.kReverse);
    }else{

    }
  }

  public void foldHatchDown(){
    if(Robot.elevator.getLimitDown().get()){
      this.HatchFold.set(Value.kForward);
    }else{

    }
  }

  public boolean getLimit(){
    return limit.get();
  }

  public void goToResetPosition(){
    while(getLimit())
    {
      //System.out.println("reseting position");
      setSpeedFolding(-0.7);
    }
    this.resetEncoder();
    setSpeedFolding(0.0);
  }

  public void resetEncoder(){
    this.encoder.reset();
  }
  

  public double getEncoerValue(){
    return Math.abs(encoder.getDistance()/100.0); 
  }

  @Override
  public void initDefaultCommand() {
    // Set the default command for a subsystem here.
    setDefaultCommand(new HandControlGripper());
  }
}
