/*----------------------------------------------------------------------------*/
/* Copyright (c) 2018 FIRST. All Rights Reserved.                             */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot.commands.CommandWithEnd;

import edu.wpi.first.wpilibj.DigitalInput;
import edu.wpi.first.wpilibj.command.Command;
import frc.robot.Robot;

public class ElevatorNEW extends Command {
  private double encoderTarget;
  private double HATCH_ONE = 2415.084;
  private double HATCH_TWO = 7887.92;
  private double BALL_THREE = 12787.0;
  private double encoderValue;
  private DigitalInput hallTarget;
  boolean finish;


  public ElevatorNEW(int goTO) {
    // Use requires() here to declare subsystem dependencies
    // eg. requires(chassis);
    requires(Robot.elevator);
    if(goTO == 1){
      encoderTarget = HATCH_ONE;
      hallTarget = Robot.elevator.getHatchOne();
    }else if(goTO == 2){
      encoderTarget = HATCH_TWO;
      hallTarget = Robot.elevator.getHatchTwo();
    }else if(goTO == 3){
      encoderTarget = BALL_THREE;
      hallTarget = Robot.elevator.getBallThree();
    }
    //System.out.println("done the con");
  }

  // Called just before this Command runs the first time
  @Override
  protected void initialize() {
    Robot.elevator.lockOff();
    System.out.println("in the init");
    System.out.println("setting raw To Zero");
    Robot.elevator.setRaw(0.0);
    System.out.println("reseting the encoder if nedded");
    if(!Robot.elevator.getLimitDown().get()){
      Robot.elevator.resetEncoder();
    }
    System.out.println("finish is eqaul to false ");
    finish = false;
    lockOff();
    System.out.println("locking off the lock");
  }

  
  // Called repeatedly when this Command is scheduled to run
  @Override
  protected void execute() {
    double k = 0.4;
    //System.out.println(Robot.elevator.getEncoderValue());
    if(!Robot.elevator.getLimitDown().get()){
      Robot.elevator.resetEncoder();
    }
    Robot.elevator.lockOff();
    encoderValue = Robot.elevator.getEncoderValue();
    double speedMotor = getSpeedByP(this.encoderTarget, encoderValue)+k;
    Robot.elevator.setRaw(speedMotor);
    if(!this.hallTarget.get()){
      finish = true;
    }
    if(Robot.oi.getXboxController().getRawButton(10) || Robot.oi.getJoystick().getRawButton(7)){
      finish = true;
      end();
    }
  }

  protected double getSpeedByP(double target,double now){
    double diff = target-now;
    double speed = diff/target;
    return speed;
  }

  // Make this return true when this Command no longer needs to run execute()
  @Override
  protected boolean isFinished() {
    if(finish){
      end();
    }else{
      Robot.elevator.lockOff();
    }
    return finish;
  }

  // Called once after isFinished returns true
  @Override
  protected void end() {
    lockOn();
    Robot.elevator.setRaw(0.0);
  }

  // Called when another command which requires one or more of the same
  // subsystems is scheduled to run
  @Override
  protected void interrupted() {
  }

  public void lockOff(){
    Robot.elevator.lockOff();
    Robot.elevator.lockOff();
    Robot.elevator.lockOff();
    Robot.elevator.lockOff();
    Robot.elevator.lockOff();
    Robot.elevator.lockOff();
    Robot.elevator.lockOff();
    Robot.elevator.lockOff();
    Robot.elevator.lockOff();
    Robot.elevator.lockOff();
    Robot.elevator.lockOff();
    Robot.elevator.lockOff();
    Robot.elevator.lockOff();
    Robot.elevator.lockOff();
    Robot.elevator.lockOff();
    Robot.elevator.lockOff();
    Robot.elevator.lockOff();
    Robot.elevator.lockOff();
    Robot.elevator.lockOff();
    Robot.elevator.lockOff();
    Robot.elevator.lockOff();
    Robot.elevator.lockOff();
    Robot.elevator.lockOff();
    Robot.elevator.lockOff();
    Robot.elevator.lockOff();
    Robot.elevator.lockOff();
    Robot.elevator.lockOff();
    Robot.elevator.lockOff();
  }

  public void lockOn(){
    Robot.elevator.lockOn();
    Robot.elevator.lockOn();
    Robot.elevator.lockOn();
    Robot.elevator.lockOn();
    Robot.elevator.lockOn();
    Robot.elevator.lockOn();
    Robot.elevator.lockOn();
    Robot.elevator.lockOn();
    Robot.elevator.lockOn();
    Robot.elevator.lockOn();
    Robot.elevator.lockOn();
    Robot.elevator.lockOn();
    Robot.elevator.lockOn();
    Robot.elevator.lockOn();
    Robot.elevator.lockOn();
    Robot.elevator.lockOn();
    Robot.elevator.lockOn();
    Robot.elevator.lockOn();
    Robot.elevator.lockOn();
    Robot.elevator.lockOn();
    Robot.elevator.lockOn();
    Robot.elevator.lockOn();
    Robot.elevator.lockOn();
    Robot.elevator.lockOn();
    Robot.elevator.lockOn();
    Robot.elevator.lockOn();
    Robot.elevator.lockOn();
    Robot.elevator.lockOn();
    Robot.elevator.lockOn();
    Robot.elevator.lockOn();
    Robot.elevator.lockOn();

  }
}
