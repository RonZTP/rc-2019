package frc.robot.commands.CommandWithEnd;

/*----------------------------------------------------------------------------*/
/* Copyright (c) 2018 FIRST. All Rights Reserved.                             */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/


import edu.wpi.first.wpilibj.command.Command;
import frc.robot.Robot;

public class ElevatorDown extends Command {
  boolean finish;
  public ElevatorDown() {
    // Use requires() here to declare subsystem dependencies
    // eg. requires(chassis);
    requires(Robot.elevator);
  }

  // Called just before this Command runs the first time
  @Override
  protected void initialize() {
    Robot.elevator.lockOff();
    Robot.gripper.foldHatchDown();
    finish = false;
  }

  // Called repeatedly when this Command is scheduled to run
  @Override
  protected void execute() {
    Robot.elevator.setRaw(-0.4);
    if(!Robot.elevator.getLimitDown().get() || (Robot.elevator.getEncoderValue() <0.0 && Robot.elevator.getEncoderValue()>100.0)){
      finish = true;
      end();
    }
    System.out.println("in the exe befor if that cancel");
    if(Robot.oi.getXboxController().getRawButton(10) || Robot.oi.getJoystick().getRawButton(7)){
      end();
      finish = true;
 
    }
  }

  // Make this return true when this Command no longer needs to run execute()
  @Override
  protected boolean isFinished() {
    if(finish){
      end();
    }
    return finish;
  }

  // Called once after isFinished returns true
  @Override
  protected void end() {
    Robot.elevator.setRaw(0.0);
    Robot.elevator.lockOff();
  }

  // Called when another command which requires one or more of the same
  // subsystems is scheduled to run
  @Override
  protected void interrupted() {
  }
}
