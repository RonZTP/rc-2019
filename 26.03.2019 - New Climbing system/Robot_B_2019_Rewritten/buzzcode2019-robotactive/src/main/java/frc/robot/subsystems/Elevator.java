/*----------------------------------------------------------------------------*/
/* Copyright (c) 2018 FIRST. All Rights Reserved.                             */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot.subsystems;

import edu.wpi.first.wpilibj.DigitalInput;
import edu.wpi.first.wpilibj.DoubleSolenoid;
import edu.wpi.first.wpilibj.Encoder;
import edu.wpi.first.wpilibj.VictorSP;
import edu.wpi.first.wpilibj.command.Subsystem;
import frc.robot.RobotMap;
import frc.robot.commands.HandControlElevator;

/**
 * Add your docs here.
 */
public class Elevator extends Subsystem {
  // Put methods for controlling this subsystem
  // here. Call these from Commands.
  private VictorSP Right;
  private VictorSP Left;

  private DigitalInput LimitDown;
  private DigitalInput hatchOne;
  private DigitalInput hatchTwo;
  private DigitalInput ballThree;

  private Encoder encoder;

  private DoubleSolenoid lock;


  public Elevator(){
    Right = new VictorSP(RobotMap.ELEVATOR_RIGHT);
    Left = new VictorSP(RobotMap.ELEVATOR_LEFT);
    Left.setInverted(true);

    setLimitDown(new DigitalInput(RobotMap.ELEVATOR_LIMIT_DOWN));
    setHatchOne(new DigitalInput(RobotMap.ELEVATOR_HATCH_ONE));
    setHatchTwo(new DigitalInput(RobotMap.ELEVATOR_HATCH_TWO));
    setBallThree(new DigitalInput(RobotMap.ELEVATOR_BALL_THREE));

    lock = new DoubleSolenoid(RobotMap.SOLENOID_DRIVING_FORWARD_CHANNEL, RobotMap.SOLENOID_DRIVING_REVERSE_CHANNEL);

    encoder = new Encoder(8,9,false,Encoder.EncodingType.k4X);
    encoder.setMaxPeriod(.1);
    encoder.setMinRate(10);
    encoder.setDistancePerPulse(5);
    encoder.setReverseDirection(true);
    encoder.setSamplesToAverage(7);
    encoder.reset();
  }

  /**
   * @return the ballThree
   */
  public DigitalInput getBallThree() {
    return ballThree;
  }

  /**
   * @param ballThree the ballThree to set
   */
  public void setBallThree(DigitalInput ballThree) {
    this.ballThree = ballThree;
  }

  public void lockOn() {
    lock.set(DoubleSolenoid.Value.kReverse);
  }

  public void lockOff(){
    lock.set(DoubleSolenoid.Value.kForward);
  }

  /**
   * @return the hatchTwo
   */
  public DigitalInput getHatchTwo() {
    return hatchTwo;
  }

  /**
   * @param hatchTwo the hatchTwo to set
   */
  public void setHatchTwo(DigitalInput hatchTwo) {
    this.hatchTwo = hatchTwo;
  }

  /**
   * @return the hatchOne
   */
  public DigitalInput getHatchOne() {
    return hatchOne;
  }

  /**
   * @param hatchOne the hatchOne to set
   */
  public void setHatchOne(DigitalInput hatchOne) {
    this.hatchOne = hatchOne;
  }

  /**
   * @return the limitDown
   */
  public DigitalInput getLimitDown() {
    return LimitDown;
  }

  /**
   * @param limitDown the limitDown to set
   */
  public void setLimitDown(DigitalInput limitDown) {
    this.LimitDown = limitDown;
  }
  
  //function returning the encoder value at the requsted moment
  public double getEncoderValue(){
    return Math.abs(encoder.getDistance()/100.0);
  }

  //function reseting the encoder value
  public void resetEncoder(){
    encoder.reset();
  }

  public void setRaw(double speed) {
    if(!this.LimitDown.get() && speed<=0.0){
      speed = 0.0;
    }
    Right.set(-speed);
    Left.set(-speed);
  }

  @Override
  public void initDefaultCommand() {
    // Set the default command for a subsystem here.
    //setDefaultCommand(new ElevatorNEW());
    setDefaultCommand(new HandControlElevator());
  }
}
