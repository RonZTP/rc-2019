/*----------------------------------------------------------------------------*/
/* Copyright (c) 2017-2018 FIRST. All Rights Reserved.                        */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot;


/**
 * The RobotMap is a mapping from the ports sensors and actuators are wired into
 * to a variable name. This provides flexibility changing wiring, makes checking
 * the wiring easier and significantly reduces the number of magic numbers
 * floating around.
 */
public class RobotMap {
  // For example to map the left and right motors, you could define the
  // following variables to use with your drivetrain subsystem.
  // public static int leftMotor = 1;
  // public static int rightMotor = 2;
  public static int DRIVE_JOYSTICK = 0;
  public static int XBOX_CONTROLLER = 1;
  public static int DRIVE_RIGHT_UP = 12;
  public static int DRIVE_RIGHT_DOWN = 11;
  public static int DRIVE_LEFT_UP = 10;
  public static int DRIVE_LEFT_DOWN = 9;
  public static int ELEVATOR_RIGHT = 2;
  public static int ELEVATOR_LEFT = 6;
  public static int ELEVATOR_LIMIT_DOWN = 0;
  public static int ELEVATOR_HATCH_ONE = 2;
  public static int ELEVATOR_HATCH_TWO = 4;
  public static int ELEVATOR_BALL_THREE = 1;
  public static int ELEVATOR_FOURTH_STAGE = 4;
  public static int GRIPPER_UP = 42;
  public static int GRIPPER_DOWN =  45;
  public static int GRIPPER_FOLDING = 13;
  public static int GRIPPER_LIMIT = 5;
  public static int HATCH_SOLENOID_FORWARD = 0; ////////////////////////////SOLENOID_DRIVING_FORWARD_CHANNEL
  public static int HATCH_SOLENOID_REVERSE = 1; /////////////////////HATCH_SOLENOID_FORWARD

  public static int COMPRESSOR = 0;

  public static int SOLENOID_DRIVING_FORWARD_CHANNEL = 6;
  public static int SOLENOID_DRIVING_REVERSE_CHANNEL = 7;
  public static int SOLENOID_LOCK_FORWARD_CHANNEL = 4;
  public static int SOLENOID_LOCK_REVERSE_CHANNEL = 5;

  // If you are using multiple modules, make sure to define both the port
  // number and the module. For example you with a rangefinder:
  // public static int rangefinderPort = 1;
  // public static int rangefinderModule = 1;
}
