/*----------------------------------------------------------------------------*/
/* Copyright (c) 2018 FIRST. All Rights Reserved.                             */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot.commands.CommandWithEnd;


import edu.wpi.first.wpilibj.DigitalInput;
import edu.wpi.first.wpilibj.Timer;
import edu.wpi.first.wpilibj.command.Command;
import frc.robot.Robot;

public class ElevatorNEW extends Command {
  private double encoderTarget;
  private double HATCH_ONE = 2415.084; //you might change theese three values (HTACH_ONE,HATCH_TWO,BALL_THREE) theese are the heights of the elevator in the encoder units
  private double HATCH_TWO = 7887.92;
  private double BALL_THREE = 12787.0;
  private double encoderValue;
  private DigitalInput hallTarget;
  private DigitalInput beforeTargetHall;
  boolean finish;
  boolean afterHall;
  Timer time;


  public ElevatorNEW(int goTO) {
    // Use requires() here to declare subsystem dependencies
    // eg. requires(chassis);
    requires(Robot.elevator);
   
    if(goTO == 1){
      encoderTarget = HATCH_ONE;
      hallTarget = Robot.elevator.getHatchOne();
    }else if(goTO == 2){
      encoderTarget = HATCH_TWO;
      hallTarget = Robot.elevator.getHatchTwo();
    }else if(goTO == 3){
      encoderTarget = BALL_THREE;
      hallTarget = Robot.elevator.getBallThree();
    }
    time = new Timer();
    //System.out.println("done the con");
  }

  // Called just before this Command runs the first time
  @Override
  protected void initialize() {
    Robot.elevator.lockOff();
    time.stop();
    time.reset();
    //System.out.println("in the init");
    //System.out.println("setting raw To Zero");
    Robot.elevator.setRaw(0.0);
    //System.out.println("reseting the encoder if nedded");
    if(!Robot.elevator.getLimitDown().get()){
      Robot.elevator.resetEncoder();
    }
    //System.out.println("finish is eqaul to false ");
    finish = false;
    lockOff();
    //System.out.println("locking off the lock");
    afterHall = false;
    time.start();

  }

  
  // Called repeatedly when this Command is scheduled to run

  /* *
   * a quick explenation about our elevator:
   * the speed of the elevator is calculated by this formuala:
   * (The Target of the elevator to reach - the current value of the elevator)/ the Target of the elevator to reach
   * this is a proportional formula 
   * the elevator slows it speed as she getting close to the target 
   * 
   * for example:
   * our target is 12000 (encoder units)
   * our current value of the encoder is : 8000
   * the speed will be: 1/3 precent
   * 
   * but as if we are too close to target the speed will be close to zero and thats not enough power
   * for the motors to lift the elevator up so the k is the minimum speed of the elevator 
   * take that in mind that the k can also control when we would like to slow down
   * */
  @Override
  protected void execute() {
    double speed = 0.6;
   

    Robot.elevator.lockOff();
    Robot.elevator.setRaw(speed);
    Robot.elevator.lockOff();
    if(!this.hallTarget.get() || Robot.oi.getXboxController().getRawButton(10) || Robot.oi.getJoystick().getRawButton(7)|| time.get()>2.9){
      finish = true;
    }
    

    /*
    double k = 0.5; //this one is controlling the minimum speed of the elevator
    System.out.println(Robot.elevator.getEncoderValue());
    if(!Robot.elevator.getLimitDown().get()){
      Robot.elevator.resetEncoder();
    }
    Robot.elevator.lockOff();
    SmartDashboard.putNumber("encoder value is:", Robot.elevator.getEncoderValue());
    encoderValue = Robot.elevator.getEncoderValue();
    double speedMotor = getSpeedByP(this.encoderTarget, encoderValue)+k;
    Robot.elevator.setRaw(speedMotor);
    if (!this.hallTarget.get()) { // TODO: HALL TARGET MIGHT BE ACTIVATED
      finish = true;
  }

  if (Robot.oi.getXboxController().getRawButton(10) || Robot.oi.getJoystick().getRawButton(7)) { // TODO: BUTTON MIGHT BE PRESSED
      finish = true;
  }
  */
  }

  protected double getSpeedByP(double target,double now){
    double diff = target-now; //the formula as explained earlier
    double speed = diff/target;
    return speed;
  }

  // Make this return true when this Command no longer needs to run execute()
  @Override
  protected boolean isFinished() {
    if(finish){
      end();
    }else{
      Robot.elevator.lockOff();
    }
    return finish;
  }

  // Called once after isFinished returns true
  @Override
  protected void end() {
    lockOn();
    Robot.elevator.setRaw(0.0);
    time.stop();
  }

  // Called when another command which requires one or more of the same
  // subsystems is scheduled to run
  @Override
  protected void interrupted() {
  }

  public void lockOff(){
    Robot.elevator.lockOff();
    Robot.elevator.lockOff();
    Robot.elevator.lockOff();
    Robot.elevator.lockOff();
    Robot.elevator.lockOff();
    Robot.elevator.lockOff();
    Robot.elevator.lockOff();
    Robot.elevator.lockOff();
    Robot.elevator.lockOff();
    Robot.elevator.lockOff();
    Robot.elevator.lockOff();
    Robot.elevator.lockOff();
    Robot.elevator.lockOff();
    Robot.elevator.lockOff();
    Robot.elevator.lockOff();
    Robot.elevator.lockOff();
    Robot.elevator.lockOff();
    Robot.elevator.lockOff();
    Robot.elevator.lockOff();
    Robot.elevator.lockOff();
    Robot.elevator.lockOff();
    Robot.elevator.lockOff();
    Robot.elevator.lockOff();
    Robot.elevator.lockOff();
    Robot.elevator.lockOff();
    Robot.elevator.lockOff();
    Robot.elevator.lockOff();
    Robot.elevator.lockOff();
  }

  public void lockOn(){
    Robot.elevator.lockOn();
    Robot.elevator.lockOn();
    Robot.elevator.lockOn();
    Robot.elevator.lockOn();
    Robot.elevator.lockOn();
    Robot.elevator.lockOn();
    Robot.elevator.lockOn();
    Robot.elevator.lockOn();
    Robot.elevator.lockOn();
    Robot.elevator.lockOn();
    Robot.elevator.lockOn();
    Robot.elevator.lockOn();
    Robot.elevator.lockOn();
    Robot.elevator.lockOn();
    Robot.elevator.lockOn();
    Robot.elevator.lockOn();
    Robot.elevator.lockOn();
    Robot.elevator.lockOn();
    Robot.elevator.lockOn();
    Robot.elevator.lockOn();
    Robot.elevator.lockOn();
    Robot.elevator.lockOn();
    Robot.elevator.lockOn();
    Robot.elevator.lockOn();
    Robot.elevator.lockOn();
    Robot.elevator.lockOn();
    Robot.elevator.lockOn();
    Robot.elevator.lockOn();
    Robot.elevator.lockOn();
    Robot.elevator.lockOn();
    Robot.elevator.lockOn();

  }
}
