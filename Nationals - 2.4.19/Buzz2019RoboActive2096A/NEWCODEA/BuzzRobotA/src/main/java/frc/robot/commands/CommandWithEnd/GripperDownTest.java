/*----------------------------------------------------------------------------*/
/* Copyright (c) 2018 FIRST. All Rights Reserved.                             */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot.commands.CommandWithEnd;

import edu.wpi.first.wpilibj.Timer;
import edu.wpi.first.wpilibj.command.Command;
import frc.robot.Robot;

public class GripperDownTest extends Command {
  boolean _down;
  boolean finish;
  Timer time;
  public GripperDownTest(boolean down) {
    // Use requires() here to declare subsystem dependencies
    // eg. requires(chassis);
    requires(Robot.gripper);
    time = new Timer();
    _down = down;
  }

  // Called just before this Command runs the first time
  @Override
  protected void initialize() {
    time.stop();
    time.reset();
    time.start();
    finish = false;
  }

  // Called repeatedly when this Command is scheduled to run
  @Override
  protected void execute() {
    if(_down){
      Robot.gripper.setSpeedFolding(-0.4);
    }else{
      Robot.gripper.setSpeedFolding(0.4);
    }
    if(time.get()>1.0)
    {
      finish = true;
    }
  }

  // Make this return true when this Command no longer needs to run execute()
  @Override
  protected boolean isFinished() {
    return finish;
  }

  // Called once after isFinished returns true
  @Override
  protected void end() {
    Robot.gripper.setSpeedFolding(0.0);
  }

  // Called when another command which requires one or more of the same
  // subsystems is scheduled to run
  @Override
  protected void interrupted() {
  }
}
