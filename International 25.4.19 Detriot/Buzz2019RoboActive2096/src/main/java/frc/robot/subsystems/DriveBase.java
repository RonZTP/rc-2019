/*----------------------------------------------------------------------------*/
/* Copyright (c) 2018 FIRST. All Rights Reserved.                             */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot.subsystems;

import com.ctre.phoenix.motorcontrol.ControlMode;
import com.ctre.phoenix.motorcontrol.can.TalonSRX;

import edu.wpi.first.wpilibj.Talon;
import edu.wpi.first.wpilibj.VictorSP;
import edu.wpi.first.wpilibj.command.Subsystem;
import frc.robot.RobotMap;
import frc.robot.commands.ArcadeDrive;

/**
 * Add your docs here.
 */
public class DriveBase extends Subsystem {
  

  private TalonSRX RightUp;   /*--------------------------------------*/
  private TalonSRX RightDown; /*Setting all the motors and their names*/
  private TalonSRX LeftUp;    /*                                      */
  private TalonSRX LeftDown;  /*--------------------------------------*/

  public DriveBase(){
    RightUp = new TalonSRX(RobotMap.RIGHT_UP.value);       /*---------------------------*/
    RightDown = new TalonSRX(RobotMap.RIGHT_DOWN.value);   /*initliazie all the motors  */
    LeftUp = new TalonSRX(RobotMap.LEFT_UP.value);         /*creating the motors        */
    LeftDown = new TalonSRX(RobotMap.LEFT_DOWN.value);     /*---------------------------*/
  }

  public void setRaw(double RightSideValue,double LeftSideValue){
    RightUp.set(ControlMode.PercentOutput,RightSideValue);      /*---------------------------------------*/
    RightDown.set(ControlMode.PercentOutput,RightSideValue);    /*setting the given speeds to the motors */
    LeftUp.set(ControlMode.PercentOutput,LeftSideValue);        /*                                       */
    LeftDown.set(ControlMode.PercentOutput,LeftSideValue);      /*---------------------------------------*/
  }

  @Override
  public void initDefaultCommand() {
    setDefaultCommand(new ArcadeDrive());
  }
}
