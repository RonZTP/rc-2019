/*----------------------------------------------------------------------------*/
/* Copyright (c) 2018 FIRST. All Rights Reserved.                             */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot.subsystems;

import edu.wpi.first.wpilibj.DigitalInput;
import edu.wpi.first.wpilibj.Encoder;
import edu.wpi.first.wpilibj.Victor;
import edu.wpi.first.wpilibj.VictorSP;
import edu.wpi.first.wpilibj.command.Subsystem;
import frc.robot.RobotMap;
import frc.robot.commands.GripperCommand;

/**
 * Add your docs here.
 */
public class Gripper extends Subsystem {
  // Put methods for controlling this subsystem
  // here. Call these from Commands.
  private VictorSP Up;
  private VictorSP Down;
  private VictorSP FoldingMotor;
  private Encoder encoder;
  private DigitalInput limit;
  //the constructor of the gripper
  public Gripper(){
    Up = new VictorSP(RobotMap.UP.value);
    Down = new VictorSP(RobotMap.DONW.value);
    FoldingMotor = new VictorSP(RobotMap.FOLDING_MOTOR.value);
    limit = new DigitalInput(5);
    encoder = new Encoder(6,7,false,Encoder.EncodingType.k4X);
    encoder.setMaxPeriod(.1);
    encoder.setMinRate(10);
    encoder.setDistancePerPulse(5);
    encoder.setReverseDirection(true);
    encoder.setSamplesToAverage(7);
    encoder.reset();
  }
  //setting speed to both of the motors that can be dound up and down
  public void setSpeed(double speed){
    Up.set(speed);
    Down.set(speed);
  }
  //setting the speed of the folding motor
  public void setSpeedForFoldingMotor(double speed){
    FoldingMotor.set(speed);
  }

  @Override
  public void initDefaultCommand() {
    // Set the default command for a subsystem here.
    setDefaultCommand(new GripperCommand());
  }

  public double getEncoderValue(){
    return Math.abs(encoder.getDistance()/100.0);
  }

  public boolean getLimitState(){
    return !limit.get();
  }

  public void resetEncoder(){
    encoder.reset();
  }

  public void goToBallThreePos(){
    while(this.getEncoderValue()<=5640.075){
      this.setSpeedForFoldingMotor(-0.8);
    }
    this.setSpeedForFoldingMotor(0.0);
    return;
  }

  public void goToBallTwoPos(){
    while(this.getEncoderValue() <= 9155.0875){
      this.setSpeedForFoldingMotor(-0.8);
    }
    this.setSpeedForFoldingMotor(0.0);
    return;
  }

  public void goToBallOnePos(){
    while(this.getEncoderValue()<=9500.0875){
      this.setSpeedForFoldingMotor(-0.8);
    }
    this.setSpeedForFoldingMotor(0.0);
    return;
  }

  public void goToStartPosition(){
    while(!this.getLimitState()){
      this.setSpeedForFoldingMotor(0.8);
    }
    this.setSpeedForFoldingMotor(0.0);
    return;
  }
}
