/*----------------------------------------------------------------------------*/
/* Copyright (c) 2018 FIRST. All Rights Reserved.                             */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot.commands;

import edu.wpi.first.wpilibj.command.Command;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import frc.robot.Robot;

public class GripperCommand extends Command {
  public GripperCommand() {
    requires(Robot.gripper);
  }

  // Called just before this Command runs the first time
  @Override
  protected void initialize() {
  }

  // Called repeatedly when this Command is scheduled to run
  @Override
  protected void execute() {
    double versaSpeed = 1.0;
    SmartDashboard.putString("at gripper?", "false");
    if(Robot.oi.getXboxController().getRawButton(1)){
      Robot.gripper.setSpeed(versaSpeed); //if we press the A button on the Xbox Controller we operating the versa 
    }else if(Robot.oi.getXboxController().getRawButton(2)){
      Robot.gripper.setSpeed(-versaSpeed); //if we press the B button on the Xbox Controller we operating the versa to the other side 
    }else if(Robot.oi.getXboxController().getRawButton(10)){
      System.out.println("reseting the encoder");
      Robot.gripper.resetEncoder(); 
    }else{
      Robot.gripper.setSpeed(0);
    }
    System.out.println("the encoder fold value is: " +Robot.gripper.getEncoderValue());
    double speedFold = 0.7; //folding //TODO add automatic folding
    if(Robot.oi.getXboxController().getRawButton(3) && !Robot.gripper.getLimitState()){
      Robot.gripper.setSpeedForFoldingMotor(speedFold);
    }else if(Robot.oi.getXboxController().getRawButton(3) && Robot.gripper.getLimitState()){
      Robot.gripper.setSpeedForFoldingMotor(0.0);
    }else if(Robot.oi.getXboxController().getRawButton(4)){
      Robot.gripper.setSpeedForFoldingMotor(-speedFold);
    }else{
      Robot.gripper.setSpeedForFoldingMotor(0.0);
    }
    if(Robot.gripper.getLimitState()){
      Robot.gripper.resetEncoder();
    }

   
    System.out.println("limit:" + Robot.gripper.getLimitState());
  }

  // Make this return true when this Command no longer needs to run execute()
  @Override
  protected boolean isFinished() {
    return false;
  }

  // Called once after isFinished returns true
  @Override
  protected void end() {
  }



  // Called when another command which requires one or more of the same
  // subsystems is scheduled to run
  @Override
  protected void interrupted() {
  }
}
