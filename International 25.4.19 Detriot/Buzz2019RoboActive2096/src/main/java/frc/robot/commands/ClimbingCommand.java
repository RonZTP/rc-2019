/*----------------------------------------------------------------------------*/
/* Copyright (c) 2018 FIRST. All Rights Reserved.                             */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot.commands;

import edu.wpi.first.wpilibj.GenericHID.Hand;
import edu.wpi.first.wpilibj.command.Command;
import frc.robot.Robot;

public class ClimbingCommand extends Command {
  public ClimbingCommand() {
    requires(Robot.Climbing); //requiring the climbing system
  }

  // Called just before this Command runs the first time
  @Override
  protected void initialize() {
  }

  // Called repeatedly when this Command is scheduled to run
  @Override
  protected void execute() {
    if(Math.abs(Robot.oi.getXboxController().getRawAxis(2))>0.5){
      Robot.Climbing.goDownOrUp(1);
    }else if(Math.abs(Robot.oi.getXboxController().getRawAxis(3))>0.5){
      Robot.Climbing.goDownOrUp(-1);
    }else if(Robot.oi.getXboxController().getRawButton(5)){
      Robot.Climbing.goDownOrUp(-1);
    }else{
      Robot.Climbing.goDownOrUp(0.0);
    }

    if(Robot.oi.getXboxController().getRawAxis(1) > 0.2){
      Robot.Climbing.driveWheels(0.8);
    }else if(Robot.oi.getXboxController().getRawAxis(5) >0.2){
      Robot.Climbing.driveWheels(-0.8);
    }else{
      Robot.Climbing.driveWheels(0);
    }
  }

  // Make this return true when this Command no longer needs to run execute()
  @Override
  protected boolean isFinished() {
    return false;
  }

  // Called once after isFinished returns true
  @Override
  protected void end() {
  }

  // Called when another command which requires one or more of the same
  // subsystems is scheduled to run
  @Override
  protected void interrupted() {
  }
}
