/*----------------------------------------------------------------------------*/
/* Copyright (c) 2017-2018 FIRST. All Rights Reserved.                        */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot;

/**
 * The RobotMap is a mapping from the ports sensors and actuators are wired into
 * to a variable name. This provides flexibility changing wiring, makes checking
 * the wiring easier and significantly reduces the number of magic numbers
 * floating around.
 */
public enum RobotMap {


    //Joystick Device number in Driver Station
    DRIVE_JOYSTICK(0),
    XBOX_CONTROLLER(1),
    //compressor id connected to the PCM
    COMPRESSOR(0),
    SOLENOID_DRIVING_FORWARD_CHANNEL(6),
    SOLENOID_DRIVING_REVERSE_CHANNEL(7),
    SOLENOID_LOCK_FORWARD_CHANNEL(4),
    SOLENOID_LOCK_REVERSE_CHANNEL(5),
    SOLENOID_GRIPPER_FORWARD_CHANNEL(0),
    SOLENOID_GRIPPER_REVERSE_CHANNEL(1),
    //gripper
    FOLDING_MOTOR(0),
    UP(7),
    DONW(8),
    //climbing
    RIGHT_WHEEL(3),
    LEFT_WHEEL(4),
    CLIMBING_MOTOR(5),
    //Elevator
    ELEVATOR_RIGHT(2),
    ELEVATOR_LEFT(6),
    //hallefectElevator
    BALL_ONE(0),
    BALL_TWO(1),
    BALL_THREE_AND_HATCH_THREE(2),
    HATCH_TWO(3),
    LIMIT_DOWN(4),
    //DriveBase Motors CAN Device ID
    RIGHT_UP(12),
    RIGHT_DOWN(11),
    LEFT_UP(10),
    LEFT_DOWN(9);
    	
	public final int value;
	
	RobotMap(int value){
		this.value = value;
    }
}
