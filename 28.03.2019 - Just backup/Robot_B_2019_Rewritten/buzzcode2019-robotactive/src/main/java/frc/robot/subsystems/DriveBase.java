/*----------------------------------------------------------------------------*/
/* Copyright (c) 2018 FIRST. All Rights Reserved.                             */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot.subsystems;

import com.ctre.phoenix.motorcontrol.ControlMode;
import com.ctre.phoenix.motorcontrol.can.TalonSRX;

import edu.wpi.first.wpilibj.DoubleSolenoid;
import edu.wpi.first.wpilibj.DoubleSolenoid.Value;
import edu.wpi.first.wpilibj.command.Subsystem;
import frc.robot.RobotMap;
import frc.robot.commands.ArcadeDrive;
/**
 * Add your docs here.
 */
public class DriveBase extends Subsystem {
  private TalonSRX RightUp;   /*--------------------------------------*/
  private TalonSRX RightDown; /*Setting all the motors and their names*/
  private TalonSRX LeftUp;    /*                                      */
  private TalonSRX LeftDown;  /*--------------------------------------*/
  private DoubleSolenoid shifter;

  public DriveBase(){
    RightUp = new TalonSRX(RobotMap.DRIVE_RIGHT_UP);       /*---------------------------*/
    RightDown = new TalonSRX(RobotMap.DRIVE_RIGHT_DOWN);   /*initliazie all the motors  */
    LeftUp = new TalonSRX(RobotMap.DRIVE_LEFT_UP);         /*creating the motors        */
    LeftDown = new TalonSRX(RobotMap.DRIVE_LEFT_DOWN  );   /*---------------------------*/

    shifter = new DoubleSolenoid(RobotMap.SOLENOID_LOCK_FORWARD_CHANNEL, RobotMap.SOLENOID_LOCK_REVERSE_CHANNEL);
  }

  public void shifterOn(){
    shifter.set(Value.kReverse);
  }

  public void shifterOff(){
    shifter.set(Value.kForward);
  }

  public void setRaw(double RightSideValue,double LeftSideValue){
    RightUp.set(ControlMode.PercentOutput,RightSideValue);      /*---------------------------------------*/
    RightDown.set(ControlMode.PercentOutput,RightSideValue);    /*setting the given speeds to the motors */
    LeftUp.set(ControlMode.PercentOutput,LeftSideValue);        /*                                       */
    LeftDown.set(ControlMode.PercentOutput,LeftSideValue);      /*---------------------------------------*/
  }

  /**
   * @return the rightUp
   */
  public TalonSRX getRightUp() {
    return RightUp;
  }

  /**
   * @return the leftDown
   */
  public TalonSRX getLeftDown() {
    return LeftDown;
  }

  /**
   * @return the leftUp
   */
  public TalonSRX getLeftUp() {
    return LeftUp;
  }

  /**
   * @return the rightDown
   */
  public TalonSRX getRightDown() {
    return RightDown;
  }
  

  @Override
  public void initDefaultCommand() {
    setDefaultCommand(new ArcadeDrive());
  }

}
