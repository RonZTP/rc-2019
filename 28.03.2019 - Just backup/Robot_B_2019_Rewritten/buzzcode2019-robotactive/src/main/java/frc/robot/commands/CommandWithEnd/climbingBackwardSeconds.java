/*----------------------------------------------------------------------------*/
/* Copyright (c) 2018 FIRST. All Rights Reserved.                             */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot.commands.CommandWithEnd;

import edu.wpi.first.wpilibj.Timer;
import edu.wpi.first.wpilibj.command.Command;
import frc.robot.Robot;

public class climbingBackwardSeconds extends Command {
  Timer time;
  boolean finish;
  boolean down;
  double t;
  public climbingBackwardSeconds(boolean directrion) {
    // Use requires() here to declare subsystem dependencies
    // eg. requires(chassis);
    requires(Robot.climbing);
    time = new Timer();
    down = directrion;
  }

  // Called just before this Command runs the first time
  @Override
  protected void initialize() {
    finish = false;
    time.stop();
    time.reset();
    time.start();
  }

  // Called repeatedly when this Command is scheduled to run
  @Override
  protected void execute() {
    if(down){
      Robot.climbing.setRaw(-1);
      t = 2.0;
    }else{
      Robot.climbing.setRaw(1);
      t = 1.3;
    }
    if(time.get()>t){
      finish = true;
    }
  }

  // Make this return true when this Command no longer needs to run execute()
  @Override
  protected boolean isFinished() {
    return finish;
  }

  // Called once after isFinished returns true
  @Override
  protected void end() {
    Robot.climbing.setRaw(0.0);
  }

  // Called when another command which requires one or more of the same
  // subsystems is scheduled to run
  @Override
  protected void interrupted() {
  }
}
