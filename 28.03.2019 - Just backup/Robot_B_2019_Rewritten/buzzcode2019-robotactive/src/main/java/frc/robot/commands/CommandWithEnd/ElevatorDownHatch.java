/*----------------------------------------------------------------------------*/
/* Copyright (c) 2018 FIRST. All Rights Reserved.                             */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot.commands.CommandWithEnd;

import edu.wpi.first.wpilibj.command.Command;
import frc.robot.Robot;

public class ElevatorDownHatch extends Command {
  private double encoder;
  private double target;
  boolean finish;
  public ElevatorDownHatch() {
    // Use requires() here to declare subsystem dependencies
    // eg. requires(chassis);
    requires(Robot.elevator);
  }

  // Called just before this Command runs the first time
  @Override
  protected void initialize() {
    encoder = Robot.elevator.getEncoderValue();
    target = encoder-1100;
    Robot.elevator.lockOff();
    finish = false;
  }

  // Called repeatedly when this Command is scheduled to run
  @Override
  protected void execute() {
    Robot.elevator.setRaw(-0.2);
    if(Robot.elevator.getEncoderValue()< target){
      finish = true;
    }
    if(Robot.oi.getXboxController().getRawButton(10) || Robot.oi.getJoystick().getRawButton(7)){
      finish = true;
      end();
    }
    
  }

  // Make this return true when this Command no longer needs to run execute()
  @Override
  protected boolean isFinished() {
    if(finish){
      end();
    }
    return finish;
  }

  // Called once after isFinished returns true
  @Override
  protected void end() {
    Robot.elevator.setRaw(0.0);
    Robot.elevator.lockOn();
  }

  // Called when another command which requires one or more of the same
  // subsystems is scheduled to run
  @Override
  protected void interrupted() {
  }
}
