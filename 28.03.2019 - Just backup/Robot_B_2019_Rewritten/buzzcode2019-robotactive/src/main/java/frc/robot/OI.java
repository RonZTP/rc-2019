/*----------------------------------------------------------------------------*/
/* Ron this is a private message that written by me the lord of Java (JK) stop panic be calm */
/* Ron this is a private message that written by me the lord of Java (JK) stop panic be calm */
/* Ron this is a private message that written by me the lord of Java (JK) stop panic be calm */
/* Ron this is a private message that written by me the lord of Java (JK) stop panic be calm */
/* Ron this is a private message that written by me the lord of Java (JK) stop panic be calm */
/* Ron this is a private message that written by me the lord of Java (JK) stop panic be calm */
/* Ron this is a private message that written by me the lord of Java (JK) stop panic be calm */
/* Ron this is a private message that written by me the lord of Java (JK) stop panic be calm */
/* Ron this is a private message that written by me the lord of Java (JK) stop panic be calm */
/* Ron this is a private message that written by me the lord of Java (JK) stop panic be calm */
/* Ron this is a private message that written by me the lord of Java (JK) stop panic be calm */
/* Ron this is a private message that written by me the lord of Java (JK) stop panic be calm */
/* Ron this is a private message that written by me the lord of Java (JK) stop panic be calm */
/* Ron this is a private message that written by me the lord of Java (JK) stop panic be calm */
/* Ron this is a private message that written by me the lord of Java (JK) stop panic be calm */
/* Ron this is a private message that written by me the lord of Java (JK) stop panic be calm */
/* Ron this is a private message that written by me the lord of Java (JK) stop panic be calm */
/*----------------------------------------------------------------------------*/

package frc.robot;

import edu.wpi.first.wpilibj.Joystick;
import edu.wpi.first.wpilibj.XboxController;
import edu.wpi.first.wpilibj.buttons.Button;
import edu.wpi.first.wpilibj.buttons.JoystickButton;
import edu.wpi.first.wpilibj.buttons.POVButton;
import frc.robot.commands.CommandWithEnd.*;

/**
 * This class is the glue that binds the controls on the physical operator
 * interface to the commands and command groups that allow control of the robot.
 */
public class OI {
  private Joystick joystick = new Joystick(RobotMap.DRIVE_JOYSTICK);
  private XboxController xController = new XboxController(RobotMap.XBOX_CONTROLLER);
  private final double JoystickDeadZone = 0.15;

  Button A = new  JoystickButton(xController, 1),
         B = new JoystickButton(xController, 2),
         X = new JoystickButton(xController, 3),
         Y = new JoystickButton(xController, 4),
         LB = new JoystickButton(xController, 5),
         R = new JoystickButton(xController,9),
         elevatorUp = new JoystickButton(joystick, 1), // Changed on the 3.3.2019
         NumberSeven = new JoystickButton(joystick, 7);

         ////////////////////////////////////////////
  POVButton zero = new POVButton(xController, 0),
            ninety = new POVButton(xController, 90),
            oneEighty = new POVButton(xController,180),
            twoSeventy = new POVButton(xController, 270);

  
  public OI(){
    A.whenPressed(new cargoBallPos());
    B.whenPressed(new BallOneRocket());
    X.whenPressed(new BallTwoRocket());
    Y.whenPressed(new BallThreeRocket());
    zero.whenPressed(new ElevatorNEW(3));
    ninety.whenPressed(new ElevatorNEW(1));
    twoSeventy.whenPressed(new ElevatorNEW(2));
    oneEighty.whenPressed(new ElevatorDown());
    R.whenPressed(new RealeseHatchElevator());
    NumberSeven.whenActive(new climbingFull());
    //LB.whenPressed(new CollectHatchFloor());
    //D.whenPressed(new downHatchAuto());
    //DownHatch.whenPressed(new downHatchAuto());
  }

  
  /*
   * this function is returning the X value of the joystick if the joystick value
   * is bigger than the deadzone we return 0 if not it will return the orginal
   * value
   */

  public double getX() {
    double raw = this.joystick.getX();
    if (Math.abs(raw) > JoystickDeadZone) {
      return raw;
    } else {
      return 0.0;
    }
  }

  /*
   * this function is returning the Y value of the joystick if the joystick value
   * is bigger than the deadzone we return 0 if not it will return the orginal
   * value
   */
  public double getY() {
    double raw = this.joystick.getY();
    if (Math.abs(raw) > JoystickDeadZone) {
      return -raw;
    } else {
      return 0.0;
    }
  }

  /*
   * this function is returning the Twist value of the joystick if the joystick
   * value is bigger than the deadzone we return 0 if not it will return the
   * orginal value
   */
  public double getTwist() {
    double raw = this.joystick.getTwist();
    if (Math.abs(raw) > JoystickDeadZone) {
      return raw;
    } else {
      return 0.0;
    }
  }

  /*
   * this function is returning the throttle value
   */
  public double getThrottle() {
    return (Math.abs((1.0 - this.joystick.getThrottle()) / -2.0));
  }

  /*
   * this function is returning the joystick
   */
  public Joystick getJoystick() {
    return this.joystick;
  }

  //xbox controller functions
  public XboxController getXboxController(){
    return xController;
  }

}
