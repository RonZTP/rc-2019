/*----------------------------------------------------------------------------*/
/* Copyright (c) 2018 FIRST. All Rights Reserved.                             */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot.commands;

import edu.wpi.first.wpilibj.command.Command;
import frc.robot.Robot;

public class HandControlClimbing extends Command {
  public HandControlClimbing() {
    // Use requires() here to declare subsystem dependencies
    // eg. requires(chassis);
    requires(Robot.climbing);
  }

  // Called just before this Command runs the first time
  @Override
  protected void initialize() {
  }

  // Called repeatedly when this Command is scheduled to run
  @Override
  protected void execute() {
    /*
    if(Robot.oi.getJoystick().getRawButton(5)){  //TODO: ADD COMMENT DIRECTIONS
      Robot.climbing.setRaw(-1.0);
    }else if(Robot.oi.getJoystick().getRawButton(6)){
      Robot.climbing.setRaw(1.0);
    }else{
      Robot.climbing.setRaw(0.0);
    }
    */
    if(Robot.oi.getJoystick().getRawButton(2)){
      Robot.climbing.turnPistonOn();
    }
    if(Robot.oi.getJoystick().getRawButton(1)){
      Robot.climbing.turnPistonOff();
    }
    
  }

  // Make this return true when this Command no longer needs to run execute()
  @Override
  protected boolean isFinished() {
    return false;
  }

  // Called once after isFinished returns true
  @Override
  protected void end() {
  }

  // Called when another command which requires one or more of the same
  // subsystems is scheduled to run
  @Override
  protected void interrupted() {
  }
}
