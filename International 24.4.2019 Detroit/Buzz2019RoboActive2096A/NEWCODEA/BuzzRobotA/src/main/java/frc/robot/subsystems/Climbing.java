/*----------------------------------------------------------------------------*/
/* Copyright (c) 2018 FIRST. All Rights Reserved.                             */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot.subsystems;

import com.ctre.phoenix.motorcontrol.ControlMode;
import com.ctre.phoenix.motorcontrol.can.VictorSPX;

import edu.wpi.first.wpilibj.DoubleSolenoid;
import edu.wpi.first.wpilibj.DoubleSolenoid.Value;
import edu.wpi.first.wpilibj.command.Subsystem;
import frc.robot.RobotMap;
import frc.robot.commands.HandControlClimbing;

/**
 * Add your docs here.
 */
public class Climbing extends Subsystem {
  // Put methods for controlling this subsystem
  // here. Call these from Commands.
  VictorSPX motor;
  DoubleSolenoid pistonClimb;
  public Climbing(){
    motor = new VictorSPX(RobotMap.CLIMBING_MOTOR);
    pistonClimb = new DoubleSolenoid(RobotMap.CLIMBNING_PISTON_FOUR, RobotMap.CLIMBNING_PISTON_FIVE);
  }
  
  public void setRaw(double speed){
    motor.set(ControlMode.PercentOutput, speed);
  }

  public void turnPistonOn(){
    pistonClimb.set(Value.kReverse);
  }

  public void turnPistonOff(){
    pistonClimb.set(Value.kForward);
  }
  @Override
  public void initDefaultCommand() {
    // Set the default command for a subsystem here.
    // setDefaultCommand(new MySpecialCommand());
    setDefaultCommand(new HandControlClimbing());
  }
}
