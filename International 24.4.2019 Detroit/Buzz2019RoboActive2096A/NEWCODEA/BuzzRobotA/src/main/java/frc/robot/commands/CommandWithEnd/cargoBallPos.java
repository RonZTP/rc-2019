/*----------------------------------------------------------------------------*/
/* Copyright (c) 2018 FIRST. All Rights Reserved.                             */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/


package frc.robot.commands.CommandWithEnd;

import edu.wpi.first.wpilibj.Timer;
import edu.wpi.first.wpilibj.command.Command;
import frc.robot.Robot;

public class cargoBallPos extends Command {
  boolean finish;
  Timer time;
  public cargoBallPos() {
    // Use requires() here to declare subsystem dependencies
    // eg. requires(chassis);
    requires(Robot.gripper);
    time = new Timer();
  }

  // Called just before this Command runs the first time
  @Override
  protected void initialize() {
    finish = false;
    System.out.println("initlaized");
    time.stop();
    time.reset();
    time.stop();
  }

  // Called repeatedly when this Command is scheduled to run
  @Override
  protected void execute() {
    Robot.gripper.setSpeedFolding(-0.8);
    //System.out.println("encoder gripper is:" + Robot.gripper.getEncoerValue());// Was 10000
    if(time.get()>3.0 || Robot.gripper.getEncoerValue() >= 10800.00 ||Robot.oi.getXboxController().getRawButton(10) || Robot.oi.getJoystick().getRawButton(7)){ //button tht cancel the command
      finish = true;
    }
  }

  // Make this return true when this Command no longer needs to run execute()
  @Override
  protected boolean isFinished() {

    return finish;
  }

  // Called once after isFinished returns true
  @Override
  protected void end() {
    System.out.println("done command cargo");
    Robot.gripper.setSpeedFolding(0.0);
  }

  // Called when another command which requires one or more of the same
  // subsystems is scheduled to run
  @Override
  protected void interrupted() {
  }
}
