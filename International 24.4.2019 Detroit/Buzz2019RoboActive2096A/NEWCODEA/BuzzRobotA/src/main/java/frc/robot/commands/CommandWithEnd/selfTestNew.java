/*----------------------------------------------------------------------------*/
/* Copyright (c) 2018 FIRST. All Rights Reserved.                             */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot.commands.CommandWithEnd;

import edu.wpi.first.wpilibj.command.CommandGroup;
import frc.robot.commands.gripperIntakeOuttake;

public class selfTestNew extends CommandGroup {
  /**
   * Add your docs here.
   */
  public selfTestNew() {
    addSequential(new driveForwardThreeSecond(true));
    addSequential(new waitTwoSeconds(true));
    addSequential(new driveForwardThreeSecond(false));
    addSequential(new waitTwoSeconds(true));
    addSequential(new ElevatorNEW(1));
    addSequential(new waitTwoSeconds(false));
    addSequential(new ElevatorDown());
    addSequential(new waitTwoSeconds(false));
    addSequential(new gripperIntakeOuttake(true));
    addSequential(new waitTwoSeconds(false));
    addSequential(new gripperIntakeOuttake(false));
    addSequential(new waitTwoSeconds(false));
    addSequential(new GripperDownTest(true));
    addSequential(new waitTwoSeconds(true));
    addSequential(new GripperDownTest(false));
    addSequential(new waitTwoSeconds(false));

    // Add Commands here:
    // e.g. addSequential(new Command1());
    // addSequential(new Command2());
    // these will run in order.

    // To run multiple commands at the same time,
    // use addParallel()
    // e.g. addParallel(new Command1());
    // addSequential(new Command2());
    // Command1 and Command2 will run in parallel.

    // A command group will require all of the subsystems that each member
    // would require.
    // e.g. if Command1 requires chassis, and Command2 requires arm,
    // a CommandGroup containing them would require both the chassis and the
    // arm.
  }
}
