/*----------------------------------------------------------------------------*/
/* Copyright (c) 2018 FIRST. All Rights Reserved.                             */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot.commands.CommandWithEnd;

import edu.wpi.first.wpilibj.Timer;
import edu.wpi.first.wpilibj.command.Command;
import frc.robot.Robot;

public class ElevatorDownHatch extends Command {
  private double encoder;
  private double target;
  boolean finish;
  Timer time;
  public ElevatorDownHatch() {
    // Use requires() here to declare subsystem dependencies
    // eg. requires(chassis);
    requires(Robot.elevator);
    time = new Timer();
  }

  // Called just before this Command runs the first time
  @Override
  protected void initialize() {
    encoder = Robot.elevator.getEncoderValue();
    target = encoder-1100; //the value you might change (1100) affects on the eleveator when the 'R' is pressed 
    Robot.elevator.lockOff();
    finish = false;
    time.stop();
    time.reset();
    time.start();
  }

  // Called repeatedly when this Command is scheduled to run
  @Override
  protected void execute() {
    Robot.elevator.setRaw(-0.2);

    /*
    if(Robot.elevator.getEncoderValue()< target){
      finish = true;
    }
    */
    if(time.get()>0.15 || Robot.oi.getXboxController().getRawButton(10) || Robot.oi.getJoystick().getRawButton(7)){
      finish = true;
      end();
    }
    
  }

  // Make this return true when this Command no longer needs to run execute()
  @Override
  protected boolean isFinished() {
    if(finish){
      end();
    }
    return finish;
  }

  // Called once after isFinished returns true
  @Override
  protected void end() {
    time.stop();
    Robot.elevator.setRaw(0.0);
    Robot.elevator.lockOn();
  }

  // Called when another command which requires one or more of the same
  // subsystems is scheduled to run
  @Override
  protected void interrupted() {
  }
}
