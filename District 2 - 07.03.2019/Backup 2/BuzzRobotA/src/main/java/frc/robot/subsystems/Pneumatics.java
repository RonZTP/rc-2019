/*----------------------------------------------------------------------------*/
/* Copyright (c) 2018 FIRST. All Rights Reserved.                             */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot.subsystems;

import edu.wpi.first.wpilibj.AnalogInput;
import edu.wpi.first.wpilibj.Compressor;
import edu.wpi.first.wpilibj.DoubleSolenoid;
import edu.wpi.first.wpilibj.command.Subsystem;
import frc.robot.RobotMap;
import frc.robot.commands.pneumatics;

/**
 * Add your docs here.
 */
public class Pneumatics extends Subsystem {
  // Put methods for controlling this subsystem
  // here. Call these from Commands.
  private Compressor compressor;
  private DoubleSolenoid driveSolenoid;
  private AnalogInput pressure;

  public Pneumatics() {
    compressor = new Compressor(RobotMap.COMPRESSOR); // initlaize the compressor
    //driveSolenoid = new DoubleSolenoid(RobotMap.SOLENOID_LOCK_FORWARD_CHANNEL, RobotMap.SOLENOID_LOCK_REVERSE_CHANNEL);
    pressure = new AnalogInput(0);
  }
  /*
   * this function is turning on the compressor
   */
  public void turnComprosserOn() {
    compressor.start();
  }

  /*
   * this function is turning off the compressor
   */
  public void turnComprosserOff() {
    compressor.stop();
  }

  /*
   * this function is changing the state of the drivebase to power mode
   */
  public void power() {
    driveSolenoid.set(DoubleSolenoid.Value.kReverse);
  }

  /*
   * this function is changing the state of the drivebase to speed mode
   */
  public void speed() {
    driveSolenoid.set(DoubleSolenoid.Value.kForward);

  }



  public double getPressure()
  {
    return 250.0 * pressure.getVoltage() / 5.0 - 25.0;
  }


  @Override
  public void initDefaultCommand() {
    // Set the default command for a subsystem here.
    setDefaultCommand(new pneumatics());
  }
}
