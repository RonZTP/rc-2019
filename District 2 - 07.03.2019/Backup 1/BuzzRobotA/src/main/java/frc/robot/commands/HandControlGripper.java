/*----------------------------------------------------------------------------*/
/* Copyright (c) 2018 FIRST. All Rights Reserved.                             */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot.commands;


import edu.wpi.first.wpilibj.command.Command;
import frc.robot.Robot;

public class HandControlGripper extends Command {
  int counter;
  public HandControlGripper() {
    // Use requires() here to declare subsystem dependencies
    // eg. requires(chassis);
    requires(Robot.gripper);
  }

  // Called just before this Command runs the first time
  @Override
  protected void initialize() {
    counter = 0;
  }

  // Called repeatedly when this Command is scheduled to run
  @Override
  protected void execute() {
    
    if(Robot.oi.getXboxController().getRawAxis(5) >0.5){ 
      Robot.gripper.setSpeedFolding(-0.7); 
    }else if(Robot.oi.getXboxController().getRawAxis(5) < -0.5){
      Robot.gripper.setSpeedFolding(0.7); 
    }else{
      Robot.gripper.setSpeedFolding(0.0);
    }
    
    
    System.out.println("encoder grippper is:" + Robot.gripper.getEncoerValue());
    
    
    if(Robot.oi.getXboxController().getRawButton(6)){
      Robot.gripper.setSpeedVersa(0.9, 0.9); 
    }else if(Math.abs(Robot.oi.getXboxController().getRawAxis(2))>0.5){
      Robot.gripper.setSpeedVersa(0.0, 1.0);
    }else if(Math.abs(Robot.oi.getXboxController().getRawAxis(3)) > 0.5){
      Robot.gripper.setSpeedVersa(-0.9, -0.9);
    }else{
      Robot.gripper.setSpeedVersa(0.0, 0.0);
    }

    
    if(Robot.oi.getXboxController().getRawButton(7)){
      Robot.gripper.foldHatchUp();
      Robot.gripper.setSpeedVersa(0.0, 1.0);

    }else if(Robot.oi.getXboxController().getRawButton(8)){
      Robot.gripper.foldHatchDown();
      
    }
    
    if(!Robot.elevator.getLimitDown().get()){
      Robot.elevator.resetEncoder();
    }

    //System.out.println(Robot.gripper.getEncoerValue());

  
    //System.out.println(Robot.elevator.getEncoderValue());
   // System.out.println(Robot.oi.getXboxController().getRawAxis(5));
  }

  // Make this return true when this Command no longer needs to run execute()
  @Override
  protected boolean isFinished() {
    return false;
  }

  // Called once after isFinished returns true
  @Override
  protected void end() {
  }

  // Called when another command which requires one or more of the same
  // subsystems is scheduled to run
  @Override
  protected void interrupted() {
  }
}
