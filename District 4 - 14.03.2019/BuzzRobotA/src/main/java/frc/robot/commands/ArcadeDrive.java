/*----------------------------------------------------------------------------*/
/* Copyright (c) 2018 FIRST. All Rights Reserved.                             */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot.commands;

import edu.wpi.first.wpilibj.command.Command;
import frc.robot.Robot;

public class ArcadeDrive extends Command {
  public ArcadeDrive() {
    // Use requires() here to declare subsystem dependencies
    // eg. requires(chassis);
    requires(Robot.drivebase);
  }

  // Called just before this Command runs the first time
  @Override
  protected void initialize() {
  }

  // Called repeatedly when this Command is scheduled to run
  @Override
  protected void execute() {
    double right = 0.0; // this the var that goes into the right side of the robot
    double left = 0.0; //// this the var that goes into the left side of the robot
    double Y = 0.0; // getting the y value from the joystick
    double X = 0.0; // getting the x value from the joystick
    double axisX = Robot.oi.getX(); //if we want to change to Z axis Robot.oi.getTwist(); or x to getX()
    double axisY = Robot.oi.getY();  
    /*
     * if the Y value of the joystick is in between 0 to 0.87 (the Cutting point) of
     * the function
     */
    if (Math.abs(axisY) >= 0 && Math.abs(axisY) <= 0.12) {
      Y = 0;
    } else {
      if (Math.abs(axisY) <= 0.22) {
      Y = -slowFuncY(axisY);
    } else
      Y = -fastFuncY(axisY);
    }

    /*
     * if the X value of the joystick is in between 0 to 0.87 (the Cutting point) of
     * the function
     */
    axisX = axisX * 0.4;  //Changed on the 3.3.19
    if (Math.abs(axisX) >= 0 && Math.abs(axisX) <= 0.12) {
      X = 0;
    } else {
      if (Math.abs(axisX) <= 0.5) {
      X = -slowFuncX(axisX);
    } else
      X = -fastFuncX(axisX);
    }

    left = X + Y;
    right = Y - X;

    if(Robot.oi.getJoystick().getRawButton(3)){
      Robot.drivebase.shifterOn();
      System.out.println("ON SPEED");
    }else if(Robot.oi.getJoystick().getRawButton(4)){
      Robot.drivebase.shifterOff();
      System.out.println("ON POWER");
    }

    //System.out.println(precntageOfSpeed); // printing the precentage of the speed
    Robot.drivebase.setRaw(-right * 0.9, left * 0.9); // setting speed to the drivebase 
  }

  // Make this return true when this Command no longer needs to run execute()
  @Override
  protected boolean isFinished() {
    return false;
  }

  // Called once after isFinished returns true
  @Override
  protected void end() {
  }

    /*
   * this is the linear expression for the slow function - Y Axis
   */
  protected double slowFuncY(double joyVal) {
    return joyVal*0.7; // (joyVal / 2.5);
  }

  /*
   * this is the linear exression for the fast function - Y Axis
   */
  protected double fastFuncY(double joyVal) {
    return ((1-0.22*0.7)/(1-0.22))*(joyVal - 0.08); // (5 * (joyVal - 0.8));
  }

   /*
   * this is the linear expression for the slow function - X Axis
   */
  protected double slowFuncX(double joyVal) {
    return joyVal; // (joyVal / 2.5);
  }

  /*
   * this is the linear exression for the fast function - X Axis
   */
  protected double fastFuncX(double joyVal) {
    return joyVal; // (5 * (joyVal - 0.8));
  }



  // Called when another command which requires one or more of the same
  // subsystems is scheduled to run
  @Override
  protected void interrupted() {
  }
}
