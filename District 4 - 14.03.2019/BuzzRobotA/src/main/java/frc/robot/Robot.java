/*----------------------------------------------------------------------------*/
/* Copyright (c) 2017-2018 FIRST. All Rights Reserved.                        */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot;

import edu.wpi.cscore.UsbCamera;
import edu.wpi.first.cameraserver.CameraServer;
import edu.wpi.first.wpilibj.TimedRobot;
import edu.wpi.first.wpilibj.command.Command;
import edu.wpi.first.wpilibj.command.Scheduler;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import frc.robot.commands.HandControlGripper;
import frc.robot.commands.pneumatics;
import frc.robot.commands.CommandWithEnd.selfTestNew;
import frc.robot.subsystems.*;
import org.opencv.core.Mat;
import org.opencv.imgproc.Imgproc;
import edu.wpi.cscore.CvSink;
import edu.wpi.cscore.CvSource;

//import edu.wpi.first.networktables.NetworkTableInstance;
//import frc.robot.commands.ArcadeDrive;

/**
 * The VM is configured to automatically run this class, and to call the
 * functions corresponding to each mode, as described in the TimedRobot
 * documentation. If you change the name of this class or the package after
 * creating this project, you must also update the build.gradle file in the
 * project.
 */
public class Robot extends TimedRobot {
  public static OI oi;
  public static DriveBase drivebase;
  public static Elevator elevator;
  public static Gripper gripper;
  public static Pneumatics pneumatics;
  public static HandControlGripper hand;
  public static pneumatics pn;
  static boolean ran;
  public static Command a;

  //public static ArcadeDrive arcadedrive; //enable only if driving isnt working in the autonomus


  /**
   * This function is run when the robot is first started up and should be
   * used for any initialization code.
   */
  @Override
  public void robotInit() {
    drivebase = new DriveBase();
    elevator = new Elevator();
    gripper = new Gripper();
    pneumatics = new Pneumatics();
    oi = new OI();
    hand = new HandControlGripper();
    pn = new pneumatics();
    //arcadedrive = new ArcadeDrive();
    
  ////////////////////////////////////////////////////////////////////
  /* option 1 TODO: CHANGE BACK TO CODE */
  /*  new Thread(() -> {
      UsbCamera camera = CameraServer.getInstance().startAutomaticCapture();
      camera.setResolution(256, 256);
      
      CvSink cvSink = CameraServer.getInstance().getVideo();
      CvSource outputStream = CameraServer.getInstance().putVideo("Blur", 256, 256);
      
      Mat source = new Mat();
      Mat output = new Mat();

      while(!Thread.interrupted()) {
          cvSink.grabFrame(source);
          Imgproc.cvtColor(source, output, Imgproc.COLOR_RGB2GRAY);
          outputStream.putFrame(output);
      }
  }).start();
/* 
////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////
  /* Option 2*/
  //boolean prevTrigger = false;
  
  new Thread(() -> {
    UsbCamera camera1 = CameraServer.getInstance().startAutomaticCapture(0);
    camera1.setFPS(7);
    camera1.setResolution(32, 32);
    CvSink cvSink1 = CameraServer.getInstance().getVideo();
    CvSource outputStream1 = CameraServer.getInstance().putVideo("Blur", 64, 64);

    Mat source1 = new Mat();
    Mat output1 = new Mat();

    UsbCamera camera2 = CameraServer.getInstance().startAutomaticCapture(1);
    camera2.setFPS(7);
    camera2.setResolution(32, 32);
    CvSink cvSink2 = CameraServer.getInstance().getVideo();
    CvSource outputStream2 = CameraServer.getInstance().putVideo("Blur1", 64, 64);
    
    Mat source2 = new Mat();
    Mat output2 = new Mat();

    while(!Thread.interrupted())
    {

        cvSink1.grabFrame(source1);
        cvSink2.grabFrame(source2);
        Imgproc.cvtColor(source1, output1, Imgproc.COLOR_RGB2GRAY);
        Imgproc.cvtColor(source2, output2, Imgproc.COLOR_RGB2GRAY);
        outputStream1.putFrame(output1);
        outputStream2.putFrame(output2);
    }
}).start();
  ////////////////////////////////////////////////////////////////////
    // chooser.addOption("My Auto", new MyAutoCommand());

    ////prevTrigger = Robot.oi.getJoystick().getTrigger();
}

  /**
   * This function is called every robot packet, no matter the mode. Use
   * this for items like diagnostics that you want ran during disabled,
   * autonomous, teleoperated and test.
   *
   * <p>This runs after the mode specific periodic functions, but before
   * LiveWindow and SmartDashboard integrated updating.
   */
  @Override
  public void robotPeriodic() {
  }

  /**
   * This function is called once each time the robot enters Disabled mode.
   * You can use it to reset any subsystem information you want to clear when
   * the robot is disabled.
   */
  @Override
  public void disabledInit() {
  }

  @Override
  public void disabledPeriodic() {
    Scheduler.getInstance().run();
  }

  /**
   * This autonomous (along with the chooser code above) shows how to select
   * between different autonomous modes using the dashboard. The sendable
   * chooser code works with the Java SmartDashboard. If you prefer the
   * LabVIEW Dashboard, remove all of the chooser code and uncomment the
   * getString code to get the auto name from the text box below the Gyro
   *
   * <p>You can add additional auto modes by adding additional commands to the
   * chooser code above (like the commented example) or additional comparisons
   * to the switch structure below with additional strings & commands.
   */
  @Override
  public void autonomousInit() {
    //if (drive != null) drive.start(); //enable only if the drive in the autonumos isnt working
    if(hand != null) hand.start();
    if(pn!= null) pn.start();
  }

  /**
   * This function is called periodically during autonomous.
   */
  @Override
  public void autonomousPeriodic() {
    Scheduler.getInstance().run();
  }

  @Override
  public void teleopInit() {
      SmartDashboard.setDefaultBoolean("run Test", false);
      ran = false;
      a = new selfTestNew();

  }

  /**
   * This function is called periodically during operator control.
   */
  @Override
  public void teleopPeriodic() {
    Scheduler.getInstance().run();
    if(SmartDashboard.getBoolean("run Test",false) && ran == false){
      a.start();
      System.out.println("Test is on@");
      SmartDashboard.setDefaultBoolean("run Test", false);
      ran = true;
    }
    
  }

  /**
   * This function is called periodically during test mode.
   */
  @Override
  public void testPeriodic() {
  }
}
