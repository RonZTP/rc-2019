/*----------------------------------------------------------------------------*/
/* Copyright (c) 2018 FIRST. All Rights Reserved.                             */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot.commands;

import edu.wpi.first.wpilibj.GenericHID.RumbleType;
import edu.wpi.first.wpilibj.command.Command;
import frc.robot.Robot;

public class HandControlElevator extends Command {
  public HandControlElevator() {
    // Use requires() here to declare subsystem dependencies
    requires(Robot.elevator);
  }

boolean isMoving;
boolean wasLocked;

  // Called just before this Command runs the first time
  @Override
  protected void initialize() {
    isMoving = false;
    wasLocked = true;
  }

  // Called repeatedly when this Command is scheduled to run
  @Override
  protected void execute() {
    if((Robot.oi.getXboxController().getRawAxis(1) > 0.5 && Robot.oi.getXboxController().getRawAxis(1) <= 1.0 )|| (Robot.oi.getXboxController().getRawAxis(1) < 0.0 && Robot.oi.getXboxController().getRawAxis(1) >=-1)){
      double speed =0.0;
      if(!Robot.elevator.getLimitDown().get() && Robot.oi.getXboxController().getRawAxis(1) >0.5){
        Robot.elevator.lockOn();
        speed = 0.0;
        Robot.oi.getXboxController().setRumble(RumbleType.kLeftRumble, 1.0);
        Robot.oi.getXboxController().setRumble(RumbleType.kRightRumble, 1.0);
      }else if(Robot.elevator.getLimitDown().get() && Robot.oi.getXboxController().getRawAxis(1) >0.5){
        Robot.elevator.lockOff();
        speed = -0.5; //going down
      }else if(Robot.elevator.getBallThree().get() && Robot.oi.getXboxController().getRawAxis(1) < -0.5){
        Robot.elevator.lockOff();
        speed = 0.7;
      }else if(!Robot.elevator.getBallThree().get() && Robot.oi.getXboxController().getRawAxis(1) < -0.5){
        Robot.elevator.lockOn();
        speed = 0.0;
        Robot.oi.getXboxController().setRumble(RumbleType.kLeftRumble, 1.0);
        Robot.oi.getXboxController().setRumble(RumbleType.kRightRumble, 1.0);
      }
      Robot.elevator.setRaw(speed);
    }else{
      Robot.elevator.setRaw(0.0);
      Robot.elevator.lockOn();
      
      Robot.oi.getXboxController().setRumble(RumbleType.kLeftRumble, 0.0);
      Robot.oi.getXboxController().setRumble(RumbleType.kRightRumble, 0.0);
    }
    //System.out.println("encoder elevator is: " + Robot.elevator.getEncoderValue());
    /*
    if(Robot.oi.getXboxController().getRawAxis(1) >0.5){ 
      if(!Robot.elevator.getLimitDown().get())
      {
        Robot.oi.getXboxController().setRumble(RumbleType.kLeftRumble, 1.0);
        Robot.oi.getXboxController().setRumble(RumbleType.kRightRumble, 1.0);
      }else{
        if (!isMoving) {
          isMoving = true;
          wasLocked = false;
          Robot.elevator.lockOff(); 
        }
      }
      Robot.elevator.setRaw(-0.7);
      +
    }else if(Robot.oi.getXboxController().getRawAxis(1) < -0.5){
      if (!isMoving) {
        isMoving = true;
        wasLocked = false;
        Robot.elevator.lockOff(); 
      }
      Robot.elevator.setRaw(+0.7);
    }else{
      Robot.elevator.setRaw(0.0);
      if (isMoving) {
        isMoving = false;
        Robot.elevator.lockOn();
      }
    }
    Robot.oi.getXboxController().setRumble(RumbleType.kLeftRumble, 0.0);
    Robot.oi.getXboxController().setRumble(RumbleType.kRightRumble, 0.0);
    */
    /*
    if(Robot.oi.getJoystick().getRawButton(6)){ //right is plus
      Robot.gripper.testMotorVictor.set(ControlMode.PercentOutput, 0.6);
      Robot.gripper.testMotorVictor1.set(ControlMode.PercentOutput, 0.6);
      
    }else{
      Robot.gripper.testMotorVictor.set(ControlMode.PercentOutput, 0.0);
      Robot.gripper.testMotorVictor1.set(ControlMode.PercentOutput, 0.0);
      */
  
  }

  // Make this return true when this Command no longer needs to run execute()
  @Override
  protected boolean isFinished() {
    return false;
  }

  // Called once after isFinished returns true
  @Override
  protected void end() {
  }

  // Called when another command which requires one or more of the same
  // subsystems is scheduled to run
  @Override
  protected void interrupted() {
  }
}
