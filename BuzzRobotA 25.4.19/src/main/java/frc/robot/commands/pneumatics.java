/*----------------------------------------------------------------------------*/
/* Copyright (c) 2018 FIRST. All Rights Reserved.                             */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot.commands;

import edu.wpi.first.wpilibj.command.Command;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import frc.robot.Robot;

public class pneumatics extends Command {
  public pneumatics() {
    requires(Robot.pneumatics);
  }

  // Called just before this Command runs the first time
  @Override
  protected void initialize() {
    //Robot.pneumatics.turnComprosserOn();
  }

  // Called repeatedly when this Command is scheduled to run
  @Override
  protected void execute() {
    if(Robot.pneumatics.getPressure() <100.0){
      Robot.pneumatics.turnComprosserOn();
    }else{
      Robot.pneumatics.turnComprosserOff();
    }

    if(Robot.oi.getJoystick().getRawButton(3)){
      Robot.drivebase.shifterOn();
      System.out.println("ON SPEED");
      SmartDashboard.putBoolean("on Power", false);
    }else if(Robot.oi.getJoystick().getRawButton(4)){
      Robot.drivebase.shifterOff();
      SmartDashboard.putBoolean("on Power", true);
      System.out.println("ON POWER");
    }
    //SmartDashboard.putNumber("pressure is:", Robot.pneumatics.getPressure());
  }

  // Make this return true when this Command no longer needs to run execute()
  @Override
  protected boolean isFinished() {
    return false;
  }

  // Called once after isFinished returns true
  @Override
  protected void end() {
  }

  // Called when another command which requires one or more of the same
  // subsystems is scheduled to run
  @Override
  protected void interrupted() {
  }
}
