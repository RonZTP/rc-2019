/*----------------------------------------------------------------------------*/
/* Copyright (c) 2018 FIRST. All Rights Reserved.                             */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot.commands.CommandWithEnd;

import edu.wpi.first.wpilibj.Timer;
import edu.wpi.first.wpilibj.command.Command;
import frc.robot.Robot;

public class driveForSeconds extends Command {
  Timer time;
  boolean finish;
  double timeToDrive;
  boolean firstTime;
  boolean fastDrive;
  public driveForSeconds(double t,boolean fast) {
    // Use requires() here to declare subsystem dependencies
    // eg. requires(chassis);
    requires(Robot.drivebase);
    time = new Timer();
    timeToDrive = t;
    fastDrive = fast;

  }

  // Called just before this Command runs the first time
  @Override
  protected void initialize() {
    time.stop();
    time.reset();
    time.start();
    finish = false;
    if(firstTime){
      while(time.get()<1.0){
      }
    }
  }

  // Called repeatedly when this Command is scheduled to run
  @Override
  protected void execute() {
    if(fastDrive){
      Robot.drivebase.setRaw(0.5, -0.5);
    }else{
      Robot.drivebase.setRaw(0.3, -0.3);
    }
    if(time.get()>timeToDrive){
      finish = true;
    }
  }

  // Make this return true when this Command no longer needs to run execute()
  @Override
  protected boolean isFinished() {
    return finish;
  }

  // Called once after isFinished returns true
  @Override
  protected void end() {
    Robot.drivebase.setRaw(0.0, 0.0);
    Robot.climbing.turnPistonOff();
  }

  // Called when another command which requires one or more of the same
  // subsystems is scheduled to run
  @Override
  protected void interrupted() {
  }
}
