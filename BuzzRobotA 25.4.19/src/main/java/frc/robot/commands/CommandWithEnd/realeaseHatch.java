/*----------------------------------------------------------------------------*/
/* Copyright (c) 2018 FIRST. All Rights Reserved.                             */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot.commands.CommandWithEnd;

import edu.wpi.first.wpilibj.Timer;
import edu.wpi.first.wpilibj.command.Command;
import frc.robot.Robot;

public class realeaseHatch extends Command {
  boolean finish;
  Timer time;
  public realeaseHatch() {
    // Use requires() here to declare subsystem dependencies
    // eg. requires(chassis);
    requires(Robot.gripper);
    time = new Timer();
  }

  // Called just before this Command runs the first time
  @Override
  protected void initialize() {
    System.out.println("in the initilaize");
    Robot.gripper.setSpeedVersa(0.0, -1.0); //setting the speed of the down motor to realease the hatch for 0.7 seconds
    time.stop();
    time.reset();
    time.start();
  }

  // Called repeatedly when this Command is scheduled to run
  @Override
  protected void execute() {
    System.out.println("in the execute");
    Robot.gripper.setSpeedVersa(0.0, -1.0); //setting the speed of the down motor to realease the hatch for 0.7 seconds
    Robot.gripper.setSpeedVersa(0.0, -1.0); //setting the speed of the down motor to realease the hatch for 0.7 seconds
    Robot.gripper.setSpeedVersa(0.0, -1.0); //setting the speed of the down motor to realease the hatch for 0.7 seconds
    Robot.gripper.setSpeedVersa(0.0, -1.0); //setting the speed of the down motor to realease the hatch for 0.7 seconds
    Robot.gripper.setSpeedVersa(0.0, -1.0); //setting the speed of the down motor to realease the hatch for 0.7 seconds
    Robot.gripper.setSpeedVersa(0.0, -1.0); //setting the speed of the down motor to realease the hatch for 0.7 seconds
    Robot.gripper.setSpeedVersa(0.0, -1.0); //setting the speed of the down motor to realease the hatch for 0.7 seconds
    Robot.gripper.setSpeedVersa(0.0, -1.0); //setting the speed of the down motor to realease the hatch for 0.7 seconds
    Robot.gripper.setSpeedVersa(0.0, -1.0); //setting the speed of the down motor to realease the hatch for 0.7 seconds
    Robot.gripper.setSpeedVersa(0.0, -1.0); //setting the speed of the down motor to realease the hatch for 0.7 seconds
    Robot.gripper.setSpeedVersa(0.0, -1.0); //setting the speed of the down motor to realease the hatch for 0.7 seconds
    Robot.gripper.setSpeedVersa(0.0, -1.0); //setting the speed of the down motor to realease the hatch for 0.7 seconds
    Robot.gripper.setSpeedVersa(0.0, -1.0); //setting the speed of the down motor to realease the hatch for 0.7 seconds
    Robot.gripper.setSpeedVersa(0.0, -1.0); //setting the speed of the down motor to realease the hatch for 0.7 seconds
    Robot.gripper.setSpeedVersa(0.0, -1.0); //setting the speed of the down motor to realease the hatch for 0.7 seconds
    Robot.gripper.setSpeedVersa(0.0, -1.0); //setting the speed of the down motor to realease the hatch for 0.7 seconds
    Robot.gripper.setSpeedVersa(0.0, -1.0); //setting the speed of the down motor to realease the hatch for 0.7 seconds
    Robot.gripper.setSpeedVersa(0.0, -1.0); //setting the speed of the down motor to realease the hatch for 0.7 seconds
    
    if(time.get()>0.7){ //this time and the time in the command "Fold hatch" must be equals
      finish = true;

    }
  }

  // Make this return true when this Command no longer needs to run execute()
  @Override
  protected boolean isFinished() {
    return finish;
  }

  // Called once after isFinished returns true
  @Override
  protected void end() {
    Robot.gripper.setSpeedVersa(0.0, 0.0);
    time.stop();
  }

  // Called when another command which requires one or more of the same
  // subsystems is scheduled to run
  @Override
  protected void interrupted() {
  }
}
