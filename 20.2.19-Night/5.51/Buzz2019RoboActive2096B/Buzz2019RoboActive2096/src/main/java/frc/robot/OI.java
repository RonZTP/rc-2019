/*----------------------------------------------------------------------------*/
/* Copyright (c) 2017-2018 FIRST. All Rights Reserved.                        */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot;

import edu.wpi.first.wpilibj.Joystick;
import edu.wpi.first.wpilibj.XboxController;
import edu.wpi.first.wpilibj.buttons.Button;
import edu.wpi.first.wpilibj.buttons.JoystickButton;
import edu.wpi.first.wpilibj.buttons.POVButton;
//import frc.robot.commands.testGripperAndElevator;

/**
 * This class is the glue that binds the controls on the physical operator
 * interface to the commands and command groups that allow control of the robot.
 */
public class OI {
  private Joystick joystick = new Joystick(RobotMap.DRIVE_JOYSTICK.value);
  private XboxController xController = new XboxController(RobotMap.XBOX_CONTROLLER.value);
  private final double JoystickDeadZone = 0.15;

  
  /*
   * this function is returning the X value of the joystick if the joystick value
   * is bigger than the deadzone we return 0 if not it will return the orginal
   * value
   */
  public double getX() {
    double raw = this.joystick.getX();
    if (Math.abs(raw) > JoystickDeadZone) {
      return raw;
    } else {
      return 0.0;
    }
  }

  /*
   * this function is returning the Y value of the joystick if the joystick value
   * is bigger than the deadzone we return 0 if not it will return the orginal
   * value
   */
  public double getY() {
    double raw = this.joystick.getY();
    if (Math.abs(raw) > JoystickDeadZone) {
      return -raw;
    } else {
      return 0.0;
    }
  }

  /*
   * this function is returning the Twist value of the joystick if the joystick
   * value is bigger than the deadzone we return 0 if not it will return the
   * orginal value
   */
  public double getTwist() {
    double raw = this.joystick.getTwist();
    if (Math.abs(raw) > JoystickDeadZone) {
      return raw;
    } else {
      return 0.0;
    }
  }

  /*
   * this function is returning the throttle value
   */
  public double getThrottle() {
    return (Math.abs((1.0 - this.joystick.getThrottle()) / -2.0));
  }

  /*
   * this function is returning the joystick
   */
  public Joystick getJoystick() {
    return this.joystick;
  }

  //xbox controller functions
  public XboxController getXboxController(){
    return xController;
  }

  
}
