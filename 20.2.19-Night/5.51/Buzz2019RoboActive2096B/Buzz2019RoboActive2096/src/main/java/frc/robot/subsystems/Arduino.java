/*----------------------------------------------------------------------------*/
/* Copyright (c) 2018 FIRST. All Rights Reserved.                             */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot.subsystems;

import java.util.Map;

import edu.wpi.first.wpilibj.SerialPort;
import edu.wpi.first.wpilibj.command.Subsystem;
import frc.robot.commands.ArduinoCommand;

/**
 * Add your docs here.
 */
public class Arduino extends Subsystem {
  private SerialPort serialPort;

  public Arduino() {
    serialPort = new SerialPort(115200, SerialPort.Port.kUSB);
  }

  public void setDataOnMap(Map objects) {
    String str = serialPort.readString();
    System.out.println(str);
    String[] XandY;
    try {
      XandY = str.split(",");
      String X = XandY[0];
      String Y = XandY[1];
      objects.replace("X", X);
      objects.replace("Y", Y);
    } catch (Exception e) {
      System.out.println("");
    }
    System.out.println("X: " + objects.get("X") + " Y: " + objects.get("Y"));
  }

  public void printData() {
    System.out.println(serialPort.readString());
  }

  @Override
  public void initDefaultCommand() {
    setDefaultCommand(new ArduinoCommand());
  }
}
