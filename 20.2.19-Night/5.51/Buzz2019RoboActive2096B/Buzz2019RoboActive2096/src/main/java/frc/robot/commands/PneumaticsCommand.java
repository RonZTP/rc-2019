/*----------------------------------------------------------------------------*/
/* Copyright (c) 2018 FIRST. All Rights Reserved.                             */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot.commands;

import edu.wpi.first.wpilibj.command.Command;
import frc.robot.Robot;

public class PneumaticsCommand extends Command {
  String state = "";

  public PneumaticsCommand() {
    requires(Robot.pneumatics); //requiring the pneumatics system
  }

  // Called just before this Command runs the first time
  @Override
  protected void initialize() {
  }

  // Called repeatedly when this Command is scheduled to run
  @Override
  protected void execute() {
    //letting the driver choose the state of the Robot
    if (Robot.oi.getJoystick().getRawButton(3)) { //if the driver pressed on the button number 3 the state will change to power
      state = "ON POWER";
      Robot.pneumatics.power();
    } else if(Robot.oi.getJoystick().getRawButton(4)) { //if the driver pressed on the button number 4 the state will change to speed
      state = "ON SPEED";
      Robot.pneumatics.speed();
    }
    System.out.println(state);

    if (Robot.oi.getJoystick().getRawButton(1) || Robot.oi.getXboxController().getRawButton(7) || Robot.pneumatics.getPressure() < 100.0) { 
      //filling the comprosser if the driver pressed the 1 button or if pressure is too low
      Robot.pneumatics.turnComprosserOn();
    } else {
      Robot.pneumatics.turnComprosserOff();
    }
    /*
    if(Robot.oi.getXboxController().getPOV() == 270){
      Robot.pneumatics.hatchOff();

    }else{
      Robot.pneumatics.hatchOn();

    }
*/

    /*
    if(Robot.oi.getXboxController().getRawButton(5)){
      Robot.pneumatics.gripperOn();
    }else if(Robot.oi.getXboxController().getRawButton(6)){
      Robot.pneumatics.gripperOff();
    }
*/
  }



  // Make this return true when this Command no longer needs to run execute()
  @Override
  protected boolean isFinished() {
    return false;
  }

  // Called once after isFinished returns true
  @Override
  protected void end() {
  }

  // Called when another command which requires one or more of the same
  // subsystems is scheduled to run
  @Override
  protected void interrupted() {
  }
}
