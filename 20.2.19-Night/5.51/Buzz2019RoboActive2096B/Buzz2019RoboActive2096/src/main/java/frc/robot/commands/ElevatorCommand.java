/*----------------------------------------------------------------------------*/
/* Copyright (c) 2018 FIRST. All Rights Reserved.                             */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot.commands;

import edu.wpi.first.wpilibj.Timer;
import edu.wpi.first.wpilibj.command.Command;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import frc.robot.Robot;

public class ElevatorCommand extends Command {
  public ElevatorCommand() {
    requires(Robot.elevator);
    // Use requires() here to declare subsystem dependencies
    // eg. requires(chassis);
  }

  // Called just before this Command runs the first time
  @Override
  protected void initialize() {
  }

  // Called repeatedly when this Command is scheduled to run
  @Override
  protected void execute() {
    double speed = 1.0;
    Timer time = new Timer();
    if(Robot.oi.getXboxController().getPOV() == 0){
      Robot.elevator.goToP(1); //going to level one
    }else if(Robot.oi.getXboxController().getPOV() == 90){
      Robot.elevator.goToP(2); //going to level 2
    }else if(Robot.oi.getXboxController().getPOV() == 180){
      Robot.elevator.goToP(3); //going to level 3
    }else if(Robot.oi.getXboxController().getPOV() == 270){
      time.start();
      Robot.elevator.goToP(4); //going to level 4
      time.stop();
    }else if(Robot.oi.getXboxController().getRawButton(5)){
      Robot.elevator.setRaw(-0.48);
    }else if(Robot.oi.getXboxController().getRawButton(6)){
      Robot.elevator.setRaw(0.5);
    }else if(Robot.oi.getXboxController().getRawButton(8)){
      Robot.pneumatics.lockingOn();
    }else if(Robot.oi.getXboxController().getRawButton(9)){
      Robot.elevator.setRaw(-0.3);
    }else{
      SmartDashboard.putString("at elevator?", "false");
      Robot.pneumatics.lockingOff();
      Robot.elevator.setRaw(0.0);
    }
    System.out.println(time.get());
    //System.out.println(Robot.elevator.getEncoderValue());
  }

  // Make this return true when this Command no longer needs to run execute()
  @Override
  protected boolean isFinished() {
    return false;
  }

  // Called once after isFinished returns true
  @Override
  protected void end() {
  }

  // Called when another command which requires one or more of the same
  // subsystems is scheduled to run
  @Override
  protected void interrupted() {
  }
}
