/*----------------------------------------------------------------------------*/
/* Copyright (c) 2018 FIRST. All Rights Reserved.                             */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot.subsystems;

import java.sql.Time;

import edu.wpi.first.wpilibj.DigitalInput;
import edu.wpi.first.wpilibj.Encoder;
import edu.wpi.first.wpilibj.Timer;
import edu.wpi.first.wpilibj.VictorSP;
import edu.wpi.first.wpilibj.GenericHID.RumbleType;
import edu.wpi.first.wpilibj.command.Subsystem;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import frc.robot.Robot;
import frc.robot.RobotMap;
import frc.robot.commands.ElevatorCommand;

/**
 * Add your docs here.
 */
public class Elevator extends Subsystem {
  // Put methods for controlling this subsystem
  // here. Call these from Commands.

  private VictorSP elevatorRight;
  private VictorSP elevatorLeft;

  private DigitalInput BallOne;
  private DigitalInput BallTwo;
  private DigitalInput BallThreeAndHatchThree;
  private DigitalInput HatchTwo;
  private DigitalInput LimitDown;

  private Encoder encoder;

  public Elevator(){
    elevatorRight = new VictorSP(RobotMap.ELEVATOR_RIGHT.value);
    elevatorLeft = new VictorSP(RobotMap.ELEVATOR_LEFT.value);
    setBallOne(new DigitalInput(RobotMap.BALL_ONE.value));
    setBallTwo(new DigitalInput(RobotMap.BALL_TWO.value));
    setBallThreeAndHatchThree(new DigitalInput(RobotMap.BALL_THREE_AND_HATCH_THREE.value));
    setHatchTwo(new DigitalInput(RobotMap.HATCH_TWO.value));
    setLimitDown(new DigitalInput(RobotMap.LIMIT_DOWN.value));
    encoder = new Encoder(8,9,false,Encoder.EncodingType.k4X);
    encoder.setMaxPeriod(.1);
    encoder.setMinRate(10);
    encoder.setDistancePerPulse(5);
    encoder.setReverseDirection(true);
    encoder.setSamplesToAverage(7);
    encoder.reset();
  }



  /**
   * @return the limitDown
   */
  public DigitalInput getLimitDown() {
    return LimitDown;
  }

  /**
   * @param limitDown the limitDown to set
   */
  public void setLimitDown(DigitalInput limitDown) {
    this.LimitDown = limitDown;
  }

  /**
   * @return the hatchTwo
   */
  public DigitalInput getHatchTwo() {
    return HatchTwo;
  }

  /**
   * @param hatchTwo the hatchTwo to set
   */
  public void setHatchTwo(DigitalInput hatchTwo) {
    this.HatchTwo = hatchTwo;
  }

  /**
   * @return the ballThreeAndHatchThree
   */
  public DigitalInput getBallThreeAndHatchThree() {
    return BallThreeAndHatchThree;
  }

  /**
   * @param ballThreeAndHatchThree the ballThreeAndHatchThree to set
   */
  public void setBallThreeAndHatchThree(DigitalInput ballThreeAndHatchThree) {
    this.BallThreeAndHatchThree = ballThreeAndHatchThree;
  }

  /**
   * @return the ballTwo
   */
  public DigitalInput getBallTwo() {
    return BallTwo;
  }

  /**
   * @param ballTwo the ballTwo to set
   */
  public void setBallTwo(DigitalInput ballTwo) {
    this.BallTwo = ballTwo;
  }

  /**
   * @return the ballOne
   */
  public DigitalInput getBallOne() {
    return BallOne;
  }

  /**
   * @param ballOne the ballOne to set
   */
  public void setBallOne(DigitalInput ballOne) {
    this.BallOne = ballOne;
  }

  public void setRaw(double speed) {
    elevatorRight.set(-speed);
    elevatorLeft.set(speed);
  }

  public void goTo(DigitalInput desired,double speed,double seconds,DigitalInput limit){
    Robot.pneumatics.lockingOff();
    Timer time = new Timer();
    while(desired.get()){
      this.setRaw(speed);
    }
    this.setRaw(0.0);
    time.start();
    while(time.get()<=seconds){
      Robot.pneumatics.lockingOn();
      SmartDashboard.putNumber("encoderLastRun:", Robot.elevator.getEncoderValue());
    }
    Robot.pneumatics.lockingOff();
    while(limit.get()){
      this.setRaw(-0.1);
    }
    Robot.pneumatics.lockingOn();
    this.setRaw(0.0);
  }

  public void goToP(int state){
    System.out.println("starting function");
    //original Ball_ONE was 3537.063
    final double BALL_ONE = 3537.063;
    final double HATCH_TWO = 6396.4812;
    final double BALL_TWO = 9064.0718;
    final double BALL_THREE__HATCH_THREE = 12614.203;
    double target = 0.0;
    DigitalInput desired;
    Robot.pneumatics.lockingOff();
    Timer time = new Timer();
    this.resetEncoder();
    switch(state){
      case 1:
        Robot.gripper.goToBallOnePos();
        target = BALL_ONE;
        desired = BallOne;
        break;
      case 2:
        target = HATCH_TWO;
        desired = HatchTwo;
        break;
      case 3:
        Robot.gripper.goToBallOnePos();
        target = BALL_TWO;
        desired = BallTwo;
        break;
      case 4:
        Robot.gripper.goToBallThreePos();
        target = BALL_THREE__HATCH_THREE;
        desired = BallThreeAndHatchThree;
        break;
      default:
        target = 0.0;
        desired = LimitDown;
        break;
    }
    double diff = target-this.getEncoderValue();
    double speed = 0.0;
    while(Math.abs(diff) > 200.0||desired.get()){
      diff = target-this.getEncoderValue();
      speed = diff/target;
      speed = speed+0.3;
      this.setRaw(speed);
      //System.out.println("diff is: " + diff);
      //System.out.println(desired.get());
      //System.out.println("still here at the while");
    }
    this.setRaw(0.0);
    Robot.pneumatics.lockingOn();
    //System.out.println("out from the while");
    time.start();
    while(time.get()<0.5){
      Robot.oi.getXboxController().setRumble(RumbleType.kLeftRumble, 0.5);
    }
    Robot.oi.getXboxController().setRumble(RumbleType.kLeftRumble, 0.0);
    time.stop();
    //Robot.pneumatics.lockingOff();
    //Robot.gripper.goToStartPosition();
  }

  public double getEncoderValue(){
    return Math.abs(encoder.getDistance()/100.0);
  }

  public void resetEncoder(){
    encoder.reset();
  }

  public void collectPosition(){
    Robot.pneumatics.lockingOff();
    Robot.gripper.goToStartPosition();
    
  }

  @Override
  public void initDefaultCommand() {
    setDefaultCommand(new ElevatorCommand());
  }
}
