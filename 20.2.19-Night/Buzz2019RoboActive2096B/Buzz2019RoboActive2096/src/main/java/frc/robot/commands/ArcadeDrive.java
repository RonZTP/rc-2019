/*----------------------------------------------------------------------------*/
/* Copyright (c) 2018 FIRST. All Rights Reserved.                             */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot.commands;

import edu.wpi.first.wpilibj.command.Command;
import frc.robot.Robot;

public class ArcadeDrive extends Command {
  public ArcadeDrive() {
    requires(Robot.driveBase);
  }

  // Called just before this Command runs the first time
  @Override
  protected void initialize() {
  }

  // Called repeatedly when this Command is scheduled to run
  @Override
  protected void execute() {
    double right = 0.0; // this the var that goes into the right side of the robot
    double left = 0.0; //// this the var that goes into the left side of the robot
    double precntageOfSpeed = Robot.oi.getThrottle(); // precntage of the speed
    double Y = 0.0; // getting the y value from the joystick
    double X = 0.0; // getting the x value from the joystick

    /*
     * if the Y value of the joystick is in between 0 to 0.87 (the Cutting point) of
     * the function
     */
    if (Math.abs(Robot.oi.getY()) >= 0 && Math.abs(Robot.oi.getY()) <= 0.87) {
      Y = -slowFunc(Robot.oi.getY());
    } else {
      Y = -fastFunc(Robot.oi.getY());
    }

    /*
     * if the X value of the joystick is in between 0 to 0.87 (the Cutting point) of
     * the function
     */
    if (Math.abs(Robot.oi.getX()) >= 0 && Math.abs(Robot.oi.getX()) <= 0.87) {
      X = -slowFunc(Robot.oi.getX());
    } else {
      X = -fastFunc(Robot.oi.getX());
    }

    left = X + Y;
    right = Y - X;

    //System.out.println(precntageOfSpeed); // printing the precentage of the speed
    Robot.driveBase.setRaw(-right * precntageOfSpeed, left * precntageOfSpeed); // setting speed to the drivebase
  }

  /*
   * this is the linear expression for the slow function
   */
  protected double slowFunc(double joyVal) {
    return (joyVal / 2.5);
  }

  /*
   * this is the linear exression for the fast function
   */
  protected double fastFunc(double joyVal) {
    return (5 * (joyVal - 0.8));
  }

  // Make this return true when this Command no longer needs to run execute()
  @Override
  protected boolean isFinished() {
    return false;
  }

  // Called once after isFinished returns true
  @Override
  protected void end() {
  }

  // Called when another command which requires one or more of the same
  // subsystems is scheduled to run
  @Override
  protected void interrupted() {
  }
}
