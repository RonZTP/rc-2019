/*----------------------------------------------------------------------------*/
/* Copyright (c) 2018 FIRST. All Rights Reserved.                             */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot.commands;

import edu.wpi.first.wpilibj.command.Command;
import frc.robot.Robot;

public class cargoBallPos extends Command {
  boolean finish;
  public cargoBallPos() {
    // Use requires() here to declare subsystem dependencies
    // eg. requires(chassis);
    requires(Robot.gripper);
  }

  // Called just before this Command runs the first time
  @Override
  protected void initialize() {
    finish = false;
    System.out.println("initlaized");
  }

  // Called repeatedly when this Command is scheduled to run
  @Override
  protected void execute() {
    Robot.gripper.setSpeedFolding(0.5);
    if(Robot.gripper.getEncoerValue() >= 2500.00){
      finish = true;
    }
    if(Robot.oi.getXboxController().getRawButton(10) || Robot.oi.getJoystick().getRawButton(7)){
      finish = true;
      end();
    }
  }

  // Make this return true when this Command no longer needs to run execute()
  @Override
  protected boolean isFinished() {
    if(finish){
      end();
    }
    return finish;
  }

  // Called once after isFinished returns true
  @Override
  protected void end() {
    Robot.gripper.setSpeedFolding(0.0);
  }

  // Called when another command which requires one or more of the same
  // subsystems is scheduled to run
  @Override
  protected void interrupted() {
  }
}
