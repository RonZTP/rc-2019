/*----------------------------------------------------------------------------*/
/* Copyright (c) 2018 FIRST. All Rights Reserved.                             */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot.commands;

import edu.wpi.first.wpilibj.DigitalInput;
import edu.wpi.first.wpilibj.Timer;
import edu.wpi.first.wpilibj.command.Command;
import frc.robot.Robot;

public class ElevatorLiftTo extends Command {

  int desired;
  double encoderTarget;
  DigitalInput target;
  final double HATCH_ONE = 2415.084;
  final double BALL_ONE = 3537.063;
  final double HATCH_TWO =  6357.4812;//original value was 6396.4812
  final double BALL_TWO = 9064.0718;
  final double BALL_THREE__HATCH_THREE = 12614.203;
  boolean finish;
  double speed;
  double P = 1.2,
         I = 0.0,
         D = 0.0;
  double derivtate;
  double integral, previous_error, setpoint = 0;
  Timer time;


  //constructor of the command
  public ElevatorLiftTo(int stage) {
    requires(Robot.elevator);
    /*
    desired = stage;
    if(desired == 1){
      encoderTarget = HATCH_ONE;
      target = Robot.elevator.getFirstStage();
    }else if(desired == 2){
      encoderTarget = HATCH_TWO;
      target = Robot.elevator.getSecondStage();
    }else if(desired == 3){
      encoderTarget = BALL_TWO;
      target = Robot.elevator.getThirdStage();
    }else if(desired == 4){
      encoderTarget = BALL_THREE__HATCH_THREE;
      target = Robot.elevator.getFourthStage();
    }else{
      encoderTarget = 0.0;
    }
    */
    time = new Timer();
    
  }



  // Called just before this Command runs the first time
  @Override
  protected void initialize() {
    time.start();
    while(time.get()<0.5){}
    Robot.elevator.lockOff();
    time.stop();
    speed = 0.0;
    finish = false;
    Robot.elevator.resetEncoder();

  }

  public void PID(){
    double diff = this.encoderTarget - Robot.elevator.getEncoderValue();
    //speed = diff/this.encoderTarget;
    diff = this.encoderTarget - Robot.elevator.getEncoderValue();
    integral+= diff*0.2;
    derivtate = (diff-this.previous_error)/0.2;
    speed = P*diff + I*this.integral +D*derivtate;    
  }

  // Called repeatedly when this Command is scheduled to run
  @Override
  protected void execute() {
    if(!Robot.elevator.getLimitDown().get()){
      Robot.elevator.resetEncoder();
    }
    PID();
    System.out.println(!Robot.elevator.getLimitDown().get());
    Robot.elevator.setRaw(speed);
    System.out.println(!this.target.get());
    if(!this.target.get()){
      finish = true;
      Robot.elevator.setRaw(0.0);
      time.stop();
      time.reset();
      time.start();
      Robot.elevator.lockOn();
      while(time.get() <0.7){
        
      }

    }
  }

  // Make this return true when this Command no longer needs to run execute()
  @Override
  protected boolean isFinished() {
    
    if(finish){
      end();
    }
    
    return finish;
  }

  // Called once after isFinished returns true
  @Override
  protected void end() {
    Robot.elevator.setRaw(0.0);

    Robot.elevator.lockOff();
  }

  // Called when another command which requires one or more of the same
  // subsystems is scheduled to run
  @Override
  protected void interrupted() {
  }
}