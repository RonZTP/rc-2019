/*----------------------------------------------------------------------------*/
/* Copyright (c) 2018 FIRST. All Rights Reserved.                             */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot.commands;

import edu.wpi.first.wpilibj.Timer;
import edu.wpi.first.wpilibj.command.Command;
import frc.robot.Robot;

public class GripperBall extends Command {
  boolean finish = false;
  Timer time;
  double BALL_ONE_TIME = 1.5;
  double BALL_TWO_TIME = 2.0;
  double BALL_THREE_TIME = 1.5;
  double targetTime;

  public GripperBall() {
    requires(Robot.gripper);
    time = new Timer();
  }

  // Called just before this Command runs the first time
  @Override
  protected void initialize() {
    Robot.gripper.goToResetPosition();
    time.reset();
    time.start();
    finish = false;
  }

  // Called repeatedly when this Command is scheduled to run
  @Override
  protected void execute() {
    Robot.gripper.setSpeedFolding(-0.6);
    if(time.get()>BALL_ONE_TIME){
      finish = true;
    }
  }

  // Make this return true when this Command no longer needs to run execute()
  @Override
  protected boolean isFinished() {
 
    System.out.println("time is: " + time.get() + " finish is: " + finish);
    if(finish)
      end();
    return finish;
  }

  // Called once after isFinished returns true
  @Override
  protected void end() {
    Robot.gripper.setSpeedFolding(0.0);
    time.stop();
    time.reset();
  }

  // Called when another command which requires one or more of the same
  // subsystems is scheduled to run
  @Override
  protected void interrupted() {
  }
}
