/*----------------------------------------------------------------------------*/
/* Copyright (c) 2018 FIRST. All Rights Reserved.                             */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot.commands;


import edu.wpi.first.wpilibj.command.Command;
import frc.robot.Robot;

public class HandControlGripper extends Command {
  public HandControlGripper() {
    requires(Robot.gripper);
  }

  // Called just before this Command runs the first time
  @Override
  protected void initialize() {
  }

  // Called repeatedly when this Command is scheduled to run
  @Override
  protected void execute() {
    System.out.println("this is axis 5: " + Robot.oi.getXboxController().getRawAxis(5));
    if(Robot.oi.getXboxController().getRawAxis(5) >0.2){ 
      Robot.gripper.setSpeedFolding(0.7); //UP //Was 0.4 before
    }else if(Robot.oi.getXboxController().getRawAxis(5) < -0.2){
      Robot.gripper.setSpeedFolding(-0.7); //Down
    }else{
      Robot.gripper.setSpeedFolding(0.0);
    }

    if(Robot.oi.getXboxController().getRawButton(6)){
      Robot.gripper.setSpeedVersa(1.0);
    }else if(Robot.oi.getXboxController().getRawAxis(3)>0.5){
      Robot.gripper.setSpeedVersa(-1.0);
    }else{
      Robot.gripper.setSpeedVersa(0.0);
    }
  
    //System.out.println(Robot.elevator.getEncoderValue());
   // System.out.println(Robot.oi.getXboxController().getRawAxis(5));
  }

  // Make this return true when this Command no longer needs to run execute()
  @Override
  protected boolean isFinished() {
    return false;
  }

  // Called once after isFinished returns true
  @Override
  protected void end() {
  }

  // Called when another command which requires one or more of the same
  // subsystems is scheduled to run
  @Override
  protected void interrupted() {
  }
}
