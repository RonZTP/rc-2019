/*----------------------------------------------------------------------------*/
/* Copyright (c) 2018 FIRST. All Rights Reserved.                             */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot.subsystems;

import com.ctre.phoenix.motorcontrol.ControlMode;
import com.ctre.phoenix.motorcontrol.can.VictorSPX;

import edu.wpi.first.wpilibj.DigitalInput;
import edu.wpi.first.wpilibj.DoubleSolenoid;
import edu.wpi.first.wpilibj.Encoder;
import edu.wpi.first.wpilibj.DoubleSolenoid.Value;
import edu.wpi.first.wpilibj.GenericHID.RumbleType;
import edu.wpi.first.wpilibj.command.Subsystem;
import frc.robot.Robot;
import frc.robot.RobotMap;
import frc.robot.commands.HandControlGripper;

/**
 * Add your docs here.
 */
public class Gripper extends Subsystem {
  // Put methods for controlling this subsystem
  // here. Call these from Commands.
  VictorSPX left;
  VictorSPX right;
  VictorSPX versaUp;
  public DigitalInput limitDown;
  public DigitalInput limitUp;

  public DoubleSolenoid hatchG;

  public Gripper(){
    left = new VictorSPX(RobotMap.GRIPPER_FOLDING_LEFT);
    right = new VictorSPX(RobotMap.GRIPPER_FOLDING_RIGHT);
    versaUp = new VictorSPX(RobotMap.VERSA_MOTOR_GRIPPER);
    limitDown = new DigitalInput(RobotMap.GRIPPER_LIMIT_DOWN);
    limitUp = new DigitalInput(RobotMap.GRIPPER_LIMIT_UP);
    hatchG = new DoubleSolenoid(RobotMap.HATCH_SOLENOID_FORWARD, RobotMap.HATCH_SOLENOID_REVERSE); //Added 13:54 19.08.19
    // limit = new DigitalInput(RobotMap.GRIPPER_LIMIT);
    // limitdowngrip = new DigitalInput(RobotMap.ELEVATOR_FOURTH_STAGE); //TODO: check with snir
    // limitUnfolding = new DigitalInput(RobotMap.GRIPPER_FOLDING);

  }

  public void expend()
  {
    hatchG.set(DoubleSolenoid.Value.kForward);
  }

  public void contract()
  {
    hatchG.set(DoubleSolenoid.Value.kReverse);
  }

  public void setSpeedVersa(double speed){
    versaUp.set(ControlMode.PercentOutput, speed);
  }

  public void setSpeedFolding(double speed){
    // if(!this.limit.get())
    // {
    //   this.resetEncoder();
    // }   
    
    if((speed<0.0 && !limitUp.get()) || (speed>0.0 && !limitDown.get())){
      speed = 0.0;
    }
    left.set(ControlMode.PercentOutput, speed);
    right.set(ControlMode.PercentOutput, -speed);
  }



  @Override
  public void initDefaultCommand() {
    // Set the default command for a subsystem here.
    setDefaultCommand(new HandControlGripper());
  }
}
