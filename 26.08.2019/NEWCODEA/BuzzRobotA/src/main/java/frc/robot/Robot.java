/*----------------------------------------------------------------------------*/
/* Copyright (c) 2017-2018 FIRST. All Rights Reserved.                        */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot;

import edu.wpi.cscore.UsbCamera;
import edu.wpi.cscore.VideoMode;
import edu.wpi.cscore.VideoSink;
import edu.wpi.cscore.VideoMode.PixelFormat;
import edu.wpi.first.cameraserver.CameraServer;
import edu.wpi.first.wpilibj.TimedRobot;
import edu.wpi.first.wpilibj.command.Command;
import edu.wpi.first.wpilibj.command.Scheduler;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import frc.robot.commands.HandControlGripper;
import frc.robot.commands.pneumatics;
import frc.robot.subsystems.*;
import org.opencv.core.Mat;
import org.opencv.imgproc.Imgproc;
import edu.wpi.cscore.CvSink;
import edu.wpi.cscore.CvSource;

//import edu.wpi.first.networktables.NetworkTableInstance;
//import frc.robot.commands.ArcadeDrive;

/**
 * The VM is configured to automatically run this class, and to call the
 * functions corresponding to each mode, as described in the TimedRobot
 * documentation. If you change the name of this class or the package after
 * creating this project, you must also update the build.gradle file in the
 * project.
 */
public class Robot extends TimedRobot {
  public static OI oi;
  public static DriveBase drivebase;
  public static Elevator elevator;
  public static Gripper gripper;
  public static Pneumatics pneumatics;
  public static HandControlGripper hand;
  public static pneumatics pn;
  static boolean ran;
  public static Command a;
  public static Climbing climbing;
  public static VideoSink serverForCamera;
  public static UsbCamera DriversCameraFront;
  public static UsbCamera DriversCameraBack;
  //public static ArcadeDrive arcadedrive; //enable only if driving isnt working in the autonomus


  /**
   * This function is run when the robot is first started up and should be
   * used for any initialization code.
   */
  @Override
  public void robotInit() {
    gripper = new Gripper();
    climbing = new Climbing();
    drivebase = new DriveBase();
    elevator = new Elevator();
    pneumatics = new Pneumatics();
    oi = new OI();
    pn = new pneumatics();

    

    DriversCameraFront = CameraServer.getInstance().startAutomaticCapture(0);
    DriversCameraBack = CameraServer.getInstance().startAutomaticCapture(1);
    /////
    DriversCameraFront.setFPS(30);
    DriversCameraFront.setExposureAuto();
    DriversCameraFront.setWhiteBalanceAuto();
    DriversCameraFront.setResolution(100 , 100);
    
    DriversCameraBack.setFPS(30);
    DriversCameraBack.setExposureAuto();
    DriversCameraBack.setWhiteBalanceAuto();
    DriversCameraBack.setResolution(100, 100);
    serverForCamera = CameraServer.getInstance().getServer();
    
  }
   
    

  /**
   * This function is called every robot packet, no matter the mode. Use
   * this for items like diagnostics that you want ran during disabled,
   * autonomous, teleoperated and test.
   *
   * <p>This runs after the mode specific periodic functions, but before
   * LiveWindow and SmartDashboard integrated updating.
   */
  @Override
  public void robotPeriodic() {
  }

  /**
   * This function is called once each time the robot enters Disabled mode.
   * You can use it to reset any subsystem information you want to clear when
   * the robot is disabled.
   */
  @Override
  public void disabledInit() {
  }

  @Override
  public void disabledPeriodic() {
    Scheduler.getInstance().run();
  }

  /**
   * This autonomous (along with the chooser code above) shows how to select
   * between different autonomous modes using the dashboard. The sendable
   * chooser code works with the Java SmartDashboard. If you prefer the
   * LabVIEW Dashboard, remove all of the chooser code and uncomment the
   * getString code to get the auto name from the text box below the Gyro
   *
   * <p>You can add additional auto modes by adding additional commands to the
   * chooser code above (like the commented example) or additional comparisons
   * to the switch structure below with additional strings & commands.
   */
  @Override
  public void autonomousInit() {
    //if (drive != null) drive.start(); //enable only if the drive in the autonumos isnt working
    /*
    if(hand != null) hand.start();
    if(pn!= null) pn.start();*/
  }

  /**
   * This function is called periodically during autonomous.
   */
  @Override
  public void autonomousPeriodic() {
    Scheduler.getInstance().run();
  }

  @Override
  public void teleopInit() {


  }

  /**
   * This function is called periodically during operator control.
   */
  @Override
  public void teleopPeriodic() {
    Scheduler.getInstance().run();
    
    //serverForCamera.setSource(DriversCameraFront);
    // if(SmartDashboard.getBoolean("run Test",false) && ran == false){
    //   a.start();
    //   System.out.println("Test is on@");
    //   SmartDashboard.setDefaultBoolean("run Test", false);
    //   ran = true;
    // }
    
  }


   /**
   * This function is called periodically during test mode.
   */
  @Override
  public void testPeriodic() {
  }
}
