package frc.robot.commands.CommandWithEnd;

import edu.wpi.first.wpilibj.Timer;

/*----------------------------------------------------------------------------*/
/* Copyright (c) 2018 FIRST. All Rights Reserved.                             */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/


import edu.wpi.first.wpilibj.command.Command;
import frc.robot.Robot;

public class ElevatorDown extends Command {
  boolean finish;
  Timer time;
  public ElevatorDown() {
    // Use requires() here to declare subsystem dependencies
    // eg. requires(chassis);
    requires(Robot.elevator);
    time = new Timer();
  }

  // Called just before this Command runs the first time
  @Override
  protected void initialize() {
    Robot.elevator.lockOff();
    finish = false;
    time.stop();
    time.reset();
    time.start();
  }

  // Called repeatedly when this Command is scheduled to run
  @Override
  protected void execute() {
    Robot.elevator.setRaw(-0.4);              //delete other statement it it causing problems (this one:(Robot.elevator.getEncoderValue() <0.0 && Robot.elevator.getEncoderValue()>100.0))
   
    if(Robot.oi.getXboxController().getRawButton(10) || Robot.oi.getJoystick().getRawButton(7) || !Robot.elevator.getLimitDown().get()  || (Robot.elevator.getEncoderValue() <0.0 && Robot.elevator.getEncoderValue()>100.0) || time.get()>2.9){
      finish = true;
    }
  }

  // Make this return true when this Command no longer needs to run execute()
  @Override
  protected boolean isFinished() {
    if(finish){
      end();
    }
    return finish;
  }

  // Called once after isFinished returns true
  @Override
  protected void end() {
    Robot.elevator.setRaw(0.0);
    Robot.elevator.lockOn();
    time.stop();
  }

  // Called when another command which requires one or more of the same
  // subsystems is scheduled to run
  @Override
  protected void interrupted() {
  }
}
